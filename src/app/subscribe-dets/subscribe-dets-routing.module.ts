import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubscribeDetsPage } from './subscribe-dets.page';

const routes: Routes = [
  {
    path: '',
    component: SubscribeDetsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscribeDetsPageRoutingModule {}
