import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubscribeDetsPageRoutingModule } from './subscribe-dets-routing.module';

import { SubscribeDetsPage } from './subscribe-dets.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubscribeDetsPageRoutingModule
  ],
  declarations: [SubscribeDetsPage]
})
export class SubscribeDetsPageModule {}
