import { URLS } from './../services/urls.service';
import { ModalController } from '@ionic/angular';
import { CartService } from './../services/cart.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { VacationComponent } from '../components/vacation/vacation.component';

@Component({
  selector: 'app-subscribe-dets',
  templateUrl: './subscribe-dets.page.html',
  styleUrls: ['./subscribe-dets.page.scss'],
})
export class SubscribeDetsPage implements OnInit {
item:any;
count:any=1;
imgUrl:any;
  constructor(private activatedRoute:ActivatedRoute,private cartService:CartService,private modalCtrl:ModalController) {
    this.activatedRoute.queryParams.subscribe(item=>{
      console.log(item)
      this.item = item
    })
    this.imgUrl = URLS.productImgUrl
   }

  ngOnInit() {
  }


  removeone(count){
    this.cartService.removeone(count)
  }
  addone(count){
    this.cartService.addone(count)
  }
  openVacationModal(id){  
    console.log(id)
    this.modalCtrl.create({
      cssClass:'vacationModal',
      component:VacationComponent,
      componentProps:{
        data:id
      }
    }).then((ModelEl)=>{
      ModelEl.present();
    })
  }

  cancelOrderOfDay(){
    
  }
}
