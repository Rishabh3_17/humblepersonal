import { Geolocation ,GeolocationOptions,Geoposition,PositionError} from '@ionic-native/geolocation/ngx';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ManualAdressComponent } from '../components/manual-adress/manual-adress.component';


declare var google;

@Component({
  selector: 'app-map-location',
  templateUrl: './map-location.page.html',
  styleUrls: ['./map-location.page.scss'],
})

export class MapLocationPage implements OnInit {
  options : GeolocationOptions;
  currentPos : Geoposition;
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  lat:any;
  long:any;
  constructor(private modalCtrl:ModalController,private geolocation: Geolocation) { }

  ngOnInit() {
    this.geolocation.getCurrentPosition().then((res)=>{
      console.log(res)
      this.addMap(res.coords.latitude,res.coords.longitude)
    }).catch((err)=>{
      console.log(err)
    })
  }
  gotoManualAdress(){
    this.modalCtrl.create({
      cssClass:'ManualAdress',
      component:ManualAdressComponent,
      componentProps:{

      }
    }).then((modelEl)=>{
      modelEl.present();
    })
  }

  addMap(lat,long){

    let latLng = new google.maps.LatLng(lat, long);
    let mapOptions = {
    center: latLng,
    zoom: 500,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker();

}  
addMarker(){

  let marker = new google.maps.Marker({
  map: this.map,
  animation: google.maps.Animation.DROP,
  position: this.map.getCenter()
  });

  let content = "<p>This is your current position !</p>";          
  let infoWindow = new google.maps.InfoWindow({
  content: content
  });

  google.maps.event.addListener(marker, 'click', () => {
  infoWindow.open(this.map, marker);
  });

}

}
