import { PanelService } from "./../../services/panel.service";
import { CartService } from "./../../services/cart.service";
import { NavParams, ModalController } from "@ionic/angular";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-edit-subscription",
  templateUrl: "./edit-subscription.component.html",
  styleUrls: ["./edit-subscription.component.scss"],
})
export class EditSubscriptionComponent implements OnInit {
  data: any;
  daysCheckBox = [
    {
      slot: "Sun",
      checked: false,
    },
    {
      slot: "Mon",
      checked: false,
    },
    {
      slot: "Tue",
      checked: false,
    },
    {
      slot: "Wed",
      checked: false,
    },
    {
      slot: "Thu",
      checked: false,
    },
    {
      slot: "Fri",
      checked: false,
    },
    {
      slot: "Sat",
      checked: false,
    },
  ];
  checked = [];
  start_date: any;
  end_date: any;
  constructor(
    private navparams: NavParams,
    private cartService: CartService,
    private modalCtrl: ModalController,
    private panelService: PanelService
  ) {
    this.data = this.navparams.get("data");
    console.log(this.data);
  }

  ngOnInit() {}

  addCheckBox(index, checkbox) {
    console.log(checkbox);
    if (checkbox.checked) {
      this.checked.push(checkbox.slot);
      console.log(this.checked);
      this.daysCheckBox[index].checked === true;
    } else {
      let indexinChecked = this.removeCheckedFromArray(checkbox);
      this.daysCheckBox[index].checked === false;
      this.checked.splice(indexinChecked, 1);
      console.log(this.checked);
    }
  }

  //Removes checkbox from array when you uncheck it
  removeCheckedFromArray(checkbox) {
    return this.checked.findIndex((category) => {
      return category === checkbox;
    });
  }

  editSubscription() {
    let data = {
      id: this.data.id,
      quantity: this.data.quantity,
      start_date: this.start_date,
      end_date: this.end_date,
      week_day: this.checked,
    };
    let product_type = "s";
    if (data.quantity <= 0) {
      let message = "Please Increase Quantity";
      this.panelService.errorToast(message);
    } else {
      this.cartService.editCartviaapi(data).subscribe(
        (res: any) => {
          console.log(res);
          this.closeModal();
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }
  removeOne() {
    this.data.quantity -= 1;
  }
  addOne() {
    this.data.quantity += 1;
  }
  closeModal() {
    this.modalCtrl.dismiss();
  }
}
