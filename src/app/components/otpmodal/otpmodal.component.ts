import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { ForgotPassComponent } from '../forgot-pass/forgot-pass.component';
import { PanelService } from 'src/app/services/panel.service';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-otpmodal',
  templateUrl: './otpmodal.component.html',
  styleUrls: ['./otpmodal.component.scss'],
})
export class OtpmodalComponent implements OnInit {
  inputData :{
    email:'',
    password:''
  };
  userData:{}
  useremail:any;
  form: FormGroup;
  constructor(private userService:UserService,private modalCtrl:ModalController,private navParams:NavParams,private authService:AuthService,private router:Router,private panelService:PanelService
    
    ) {
      this.inputData= this.navParams.get('formData')
      console.log(this.inputData)
   }

  ngOnInit() {
    this.form = new FormGroup({
      password: this.panelService.validatorsFunction()
    })
  }
  gotoForgotPassword(){
    this.modalCtrl.dismiss()
    this.modalCtrl.create({
      cssClass:'forgotPass',
      component:ForgotPassComponent,
      componentProps:{
        resetdata:this.inputData.email
      }
    }).then((modelEL)=>{
      modelEL.present()
      this.useremail = this.inputData.email
    console.log(this.useremail)
    const forgotData = new FormData()
    forgotData.append('email_or_mobile',this.useremail)
    console.log(forgotData)
      this.userService.forgotPass(forgotData).subscribe((res)=>{
        console.log(res)
        let success = 'Otp Sent To You Email/Mobile'
        this.panelService.successToast(success)
      },(err:any)=>{
        this.panelService.errorToast(err)
      })
      this.panelService.presentLoading();
    })
  }
  logForm(f){
    if(this.form.valid){
      console.log(this.inputData)
      this.authService.userSignin(this.inputData).subscribe((res:any)=>{
        this.userData = res.data;
      this.userService.saveUsersData(this.userData);
      this.modalCtrl.dismiss();
      this.userService.modifyAuthState();
      let success = 'Successfully Login !'
        this.panelService.successToast(success)
      // console.log(this.userService.authenticationState)
      // console.log(this.userService.authenticationState.value);
      this.router.navigate(['/main-page/home'],{
        queryParams:this.userData
      })
      this.panelService.presentLoading();
    },(err:any)=>{
      console.log(err)
      let error = err.error
      this.panelService.errorToast(error)
      console.log(err)
    })
  }else{
    this.form.markAllAsTouched()
  }
  } 
  gotologin(){
    this.modalCtrl.dismiss();
  }
  gotoMyCart(){}
}
