import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { PanelService } from 'src/app/services/panel.service';
import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-verifyotp',
  templateUrl: './verifyotp.component.html',
  styleUrls: ['./verifyotp.component.scss'],
})
export class VerifyotpComponent implements OnInit {
  OtpData={
    email:'',
    otp:'',
    otp_mobile:'',
  }
  otpResend={
    email:''
  }
  form :FormGroup;
  constructor(private auhtService:AuthService,private router:Router,private navparams:NavParams,private panelService:PanelService,private modalCtrl:ModalController) { 
    this.OtpData.email=this.navparams.get('email')
    console.log(this.OtpData)
  }


  ngOnInit() {
    this.form = new FormGroup({
      otp_email:this.panelService.validatorsFunction(),
      otp_mobile:this.panelService.validatorsFunction()
    }) 
  }
  gotoRegister(){
    this.modalCtrl.dismiss()
  }
  gotoHome(){}
  logForm(f){
    if(this.form.valid){
      this.auhtService.otpVerify(this.OtpData).subscribe((response)=>{
        console.log(response)
      // this.panelService.presentLoading()
      let success = 'otp Verified Successfully'
      this.panelService.successToast(success)
      this.modalCtrl.dismiss()
      this.router.navigateByUrl('login')
    },(err:any)=>{
      console.log(err)
      let error = err.error
      this.panelService.errorToast(error)
    })
    console.log(this.OtpData)
  }else{
    this.form.markAllAsTouched()
  }
  }


  resendOTp(type){
    this.otpResend.email = this.OtpData.email
    this.auhtService.reSendOtp(this.otpResend,type).subscribe((response)=>{
      console.log(response)
      // this.panelService.presentLoading()
    },(err:any)=>{
      console.log(err)
      // this.panelService.presentToast(err)
    })
  }
}
