import { PanelService } from 'src/app/services/panel.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-vacation',
  templateUrl: './vacation.component.html',
  styleUrls: ['./vacation.component.scss'],
})
export class VacationComponent implements OnInit {
  end_date:any;
  start_date:any;
  id:any;
  constructor(private router:Router ,private navParams:NavParams, private modelCtrl: ModalController,private panelService:PanelService) {
    this.id = this.navParams.get('data') 
    console.log(this.id)
   }

  ngOnInit() {}
  
  openMyVacation(){
    // this.panelService.presentLoading()
    console.log('ishu bhai badshah')
    console.log(this.start_date)
    let startdate = moment(this.start_date).format("MM/DD/YYYY")
    let enddate = moment(this.end_date).format("MM/DD/YYYY")
    let data={
      order_details_id:this.id,
      start_date:startdate,
      end_date:enddate
    }
    this.panelService.takeVacation(data).subscribe((res:any)=>{
      console.log(res)
      this.router.navigateByUrl('myvacation')
      // this.panelService.dismissLoading()
      this.modelCtrl.dismiss()
    },(err:any)=>{
      // this.panelService.dismissLoading()
      console.log(err)
      this.panelService.errorToast(err.error.message)
    })
    
    
  }
}
