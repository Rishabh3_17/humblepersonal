import { PanelService } from 'src/app/services/panel.service';
import { AddAddressComponent } from './../add-address/add-address.component';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-address',
  templateUrl: './show-address.component.html',
  styleUrls: ['./show-address.component.scss'],
})
export class ShowAddressComponent implements OnInit {
address:any;
  constructor(private modalCtrl:ModalController,private panelService:PanelService) {
    this.getAddress()
   }

  ngOnInit() {}

  openaddAddress(){
    this.modalCtrl.create({
      cssClass:'addAddress',
      component:AddAddressComponent,
      componentProps:{}
    }).then((modelEl)=>{
      modelEl.present()
      modelEl.onWillDismiss().then((data)=>{
        console.log(data)
        this.getAddress()
      })
    })
  }


  selectAdress(item){
    let data = {
      id:item.id,
      name:item.name,
      address:`${item.house_no},${item.floor} ${item.address_line_one} ${item.address_line_two}`
    }
    this.modalCtrl.dismiss(data);
  }

  getAddress(){
    // this.panelService.presentLoading()
    this.panelService.getAddress().subscribe((res:any)=>{
      console.log(res)
      // this.panelService.dismissLoading()
      let message = "Select One Adress"
      this.panelService.successToast(message)
      this.address = res.billing_address.data
    },(err)=>{
      // this.panelService.dismissLoading()
      console.log(err)
      this.panelService.errorToast(err.error.errors.error)
    })
  }

  closeModal(){
    this.modalCtrl.dismiss()
  }
}

