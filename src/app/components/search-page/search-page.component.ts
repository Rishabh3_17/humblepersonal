import {  ModalController } from '@ionic/angular';
import { Component, NgZone, OnInit } from '@angular/core';

declare var google: any;

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent implements OnInit {
  locationData = [];
  locationService:any;
  geocoder: any;
  constructor(private zone:NgZone,public modalCtrl:ModalController) { }

  ngOnInit() {   
    this.locationService = new google.maps.places.AutocompleteService;
    this.geocoder = new google.maps.Geocoder;
  }


  onLocationSearchChange(ev){
    let value = ev.target.value;
    if(value && value.trim() != ''){
      let config = {
        input: value,
        componentRestrictions: { country: 'IND' }
      }
      this.locationService.getPlacePredictions(config, (predictions, status)=>{
        if(predictions){
          this.locationData = [];
          this.zone.runOutsideAngular(()=>{
            this.zone.run(()=>{
              predictions.forEach((prediction)=>{
                this.locationData.push(prediction);
              });
            });
          });
        }
      });
    }else{
      this.locationData = [];
    }
  }


  onLocation(location){
    this.geocoder.geocode({
      "placeId": location.place_id
    }, (result, status)=>{
      if(status == google.maps.GeocoderStatus.OK){
        if(result[0]){
          let lat = result[0].geometry.location.lat();
          let lng = result[0].geometry.location.lng(); 
          this.modalCtrl.dismiss({ address:result[0].formatted_address, lat: lat, lng: lng});
        }
      }
    });
  }
}
