import { AddAddressComponent } from './add-address/add-address.component';
import { MapComponent } from './map/map.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { EditSubscriptionComponent } from './edit-subscription/edit-subscription.component';
import { VacationComponent } from 'src/app/components/vacation/vacation.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { OrderplacedComponent } from './orderplaced/orderplaced.component';
import { ManualAdressComponent } from './manual-adress/manual-adress.component';
import { CoupensComponent } from 'src/app/components/coupens/coupens.component';
import { AddtocartComponent } from './addtocart/addtocart.component';
import { VerifyotpComponent } from './verifyotp/verifyotp.component';
import { OtpmodalComponent } from './otpmodal/otpmodal.component';
import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
import { OffersComponent } from './offers/offers.component';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';
import { ShowAddressComponent } from './show-address/show-address.component';




@NgModule({
  declarations: [    
    VerifyotpComponent,
    ForgotPassComponent,
    OffersComponent,
    OtpmodalComponent,
    AddtocartComponent,
    CoupensComponent,
    ManualAdressComponent,
    OrderplacedComponent,
    SubscribeComponent,
    VacationComponent,
    MapComponent,
    EditSubscriptionComponent,
    SearchPageComponent,
    ShowAddressComponent,
    AddAddressComponent
 ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    IonicModule,  
    CommonModule
  ] ,
  exports: [
    VerifyotpComponent,
    ForgotPassComponent,
    OffersComponent,
    OtpmodalComponent,
    AddtocartComponent,
    CoupensComponent,
    ManualAdressComponent,
    OrderplacedComponent,
    SubscribeComponent,
    MapComponent,
    VacationComponent,
    EditSubscriptionComponent,
    SearchPageComponent,
    ShowAddressComponent,
    AddAddressComponent
  ]
})
export class ComponentsModule { }
