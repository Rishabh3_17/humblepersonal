import { PanelService } from "src/app/services/panel.service";
import { ModalController } from "@ionic/angular";
import { FormGroup } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { MapComponent } from "../map/map.component";
import { ValueAccessor } from "@ionic/angular/directives/control-value-accessors/value-accessor";
import { ShowAddressComponent } from "../show-address/show-address.component";

@Component({
  selector: "app-add-address",
  templateUrl: "./add-address.component.html",
  styleUrls: ["./add-address.component.scss"],
})
export class AddAddressComponent implements OnInit {
  form: FormGroup;
  data:any;
  constructor(
    private modalCtrl: ModalController,
    private panelService: PanelService
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      full_name: this.panelService.validatorsFunction(),
      mobile_number: this.panelService.validatorsFunction(),
      alternate_mobile_number: this.panelService.validatorsFunction(),
      house_no: this.panelService.validatorsFunction(),
      floor: this.panelService.validatorsFunction(),
      address_line_1: this.panelService.validatorsFunction(),
      address_line_2: this.panelService.validatorsFunction(),
      postal_code: this.panelService.validatorsFunction(),
    });
  }

  logForm() {
    if (this.form.valid) {
      if(this.data === undefined){
        let message = "Please Select Address From Map"
        this.panelService.errorToast(message)
      }else{     
        let value = this.form.value;
        if(value.mobile_number === value.alternate_mobile_number){
          let message = "Mobile Number And Alternate Mobile Number Can`t Be Same";
          this.panelService.errorToast(message)
        }else{        
      let data = {
        full_name: value.full_name,
        mobile: value.mobile_number,
        alternate_mobile: value.alternate_mobile_number,
        house_no: value.house_no,
        floor: value.floor,
        address_line_1: value.address_line_1,
        address_line_2: value.address_line_2,
        postal_code: value.postal_code,
        latitude:this.data.lat,
        longitude:this.data.lng 
      };
      this.panelService.presentLoading()
      this.panelService.addAddress(data).subscribe((res:any)=>{
        console.log(res)
        this.panelService.dismissLoading()
        this.panelService.successToast(res.message)
        this.modalCtrl.dismiss()
      },(err:any)=>{
        this.panelService.dismissLoading()
        let message  = "address not added Succesfully"
        this.panelService.errorToast(message)
        
        console.log(err)
      })
      console.log(this.form);
      console.log("form is valid");
    }
  }
    } else {
      console.log("form is invalid");
      this.form.markAllAsTouched();
    }
  }

  gotomap() {
    this.modalCtrl
      .create({
        cssClass: "mapComponent",
        component: MapComponent,
        componentProps: {},
      })
      .then((modelEl) => {
        modelEl.present();
        modelEl.onWillDismiss().then((data) => {
          console.log(data);
          this.data = data.data;
          console.log(this.data.lat)
      
        });
      });
  }

  modalClose(){
    this.modalCtrl.dismiss()
  }
}
