import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orderplaced',
  templateUrl: './orderplaced.component.html',
  styleUrls: ['./orderplaced.component.scss'],
})
export class OrderplacedComponent implements OnInit {

  constructor(private router:Router,private modalCtrl:ModalController) { }

  ngOnInit() {}
  
  gotoHome(){ 
    this.modalCtrl.dismiss()
    this.router.navigateByUrl('main-page/home')
  }

}
