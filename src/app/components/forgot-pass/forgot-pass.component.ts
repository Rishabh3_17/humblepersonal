import { UserService } from "./../../services/user.service";
import { ModalController } from "@ionic/angular";
import { Component, OnInit } from "@angular/core";
import { PanelService } from "src/app/services/panel.service";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "app-forgot-pass",
  templateUrl: "./forgot-pass.component.html",
  styleUrls: ["./forgot-pass.component.scss"],
})
export class ForgotPassComponent implements OnInit {
  form: FormGroup;
  email: any;
  newPassword = {
    otp: "",
    new_password: "",
    confirm_password: "",
  };
  constructor(
    private userService: UserService,
    private panelService: PanelService,
    private modalCtrl:ModalController
  ) {
    // this.email= this.navParams.get('resetdata')
    //   console.log(this.email)
  }

  ngOnInit() {
    this.form = new FormGroup({
      otp: this.panelService.validatorsFunction(),
      new_password: this.panelService.validatorsFunction(),
      confirm_password: this.panelService.validatorsFunction(),
    });
  }

  sentOtp() {
    if (this.email.length === 0) {
      let err = "Please Enter Email";
      this.panelService.errorToast(err);
    } else {
      const forgotData = new FormData();
      forgotData.append("email_or_mobile", this.email);
      console.log(forgotData);
      this.userService.forgotPass(forgotData).subscribe(
        (res) => {
          console.log(res);
          this.panelService.successToast(res['message']) 
        },
        (err: any) => {
          console.log(err);
          this.panelService.errorToast(err.error.errors['message'])
        }
      );
    }
  }

  logForm(f) {
    // this.panelService.presentLoading()
    console.log("ye chal rha h");
    console.log(this.form.value.otp);
    console.log(this.form.value.new_password);
    console.log(this.form.value.confirm_password);
    console.log(this.form.valid)
    if (this.form.valid) {
      console.log("form valid h");
      const resetPass = new FormData();
      resetPass.append("email_or_mobile", this.email);
      resetPass.append("otp", this.newPassword.otp);
      resetPass.append("new_password", this.newPassword.new_password);
      resetPass.append("confirm_password", this.newPassword.confirm_password);
      console.log(resetPass);
      
      this.userService.resetPassword(resetPass).subscribe(
        (res: any) => {
          console.log(res);
          // this.panelService.dismissLoading()
        },
        (err: any) => {
          // this.panelService.dismissLoading()
          console.log(err);
        }
      );
      // this.panelService.presentLoading();
    } else {
      console.log("else chal rha h");
      this.form.markAllAsTouched();
    }
  }
  closeModal(){
    this.modalCtrl.dismiss()
  }
}
