import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss'],
})
export class SubscribeComponent implements OnInit {

  constructor(private router:Router,private modalCtrl: ModalController) { }

  ngOnInit() {}

  gotomyCart(){
    this.modalCtrl.dismiss()
    this.router.navigateByUrl('my-cart')
  }
  
}
