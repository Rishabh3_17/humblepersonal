import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CoupensComponent } from './coupens.component';

describe('CoupensComponent', () => {
  let component: CoupensComponent;
  let fixture: ComponentFixture<CoupensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoupensComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CoupensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
