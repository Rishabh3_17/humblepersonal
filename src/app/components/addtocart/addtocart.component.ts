import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-addtocart',
  templateUrl: './addtocart.component.html',
  styleUrls: ['./addtocart.component.scss'],
})
export class AddtocartComponent implements OnInit {

  constructor(private router : Router , private modalCtrl: ModalController) { }

  ngOnInit() {}
  gotoMyCart(){
    this.modalCtrl.dismiss()
    this.router.navigateByUrl('my-cart')
  }

  gotoHome(){
    this.modalCtrl.dismiss()
    this.router.navigateByUrl('main-page/home')
  }
}
