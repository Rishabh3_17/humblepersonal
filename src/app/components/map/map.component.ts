import { SearchPageComponent } from './../search-page/search-page.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { PanelService } from 'src/app/services/panel.service';
import { UserService } from 'src/app/services/user.service';
import { Subscription } from 'rxjs';



declare var google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  @ViewChild("map") mapElement: ElementRef;
  map: any;
  geocoder: any;
  markers = [];
  paramsData;
  userUid;
  groupKey;
  groupMemberSubsrciption: Subscription;
  constructor(    
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private geolocation: Geolocation,
    private panelService: PanelService,
    private userService: UserService) { 
    
  }

  ngOnInit() {
    this.loadMap();

  }



  checkMap() {
    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        console.log(resp);
      })
      .catch((error) => {
        console.log("Error getting location", error);
      });

    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
    });
  }

  loadMap() {
    this.geolocation
      .getCurrentPosition()
      .then((res) => {
        console.log(res);
        let latLng = new google.maps.LatLng(
          res.coords.latitude,
          res.coords.longitude
        );
        let mapOptions = {
          center: latLng,
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
        };

        this.map = new google.maps.Map(
          this.mapElement.nativeElement,
          mapOptions
        );
        this.addMarker(latLng);
        // this.clearMarker();
        this.geocoder = new google.maps.Geocoder();


        google.maps.event.addListener(this.map, "click", (event) => {
          this.clearMarker();
          this.addMarker(event.latLng);

          this.geocoder.geocode(
            {
              latLng: event.latLng,
            },
            (result, status) => {
              if (status == google.maps.GeocoderStatus.OK) {
                if (result[0]) {
                  // console.log(result[0]);
                  let lat = result[0].geometry.location.lat();
                  let lng = result[0].geometry.location.lng();
                  this.saveLocation(result[0].formatted_address, lat, lng);
                }
              }
            }
          );
        });
      })
      .catch((error) => {
        // console.log(error);
        this.alert("Alert!", "Please allow your location");
      });
  }

  private addMarker(latLng) {
    var marker = new google.maps.Marker({
      map: this.map,
      position: latLng,
    });
    this.markers.push(marker);
  }

  private clearMarker() {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }

  onSearchLocation() {
    const modal = this.modalCtrl
      .create({
        cssClass: "SearchPage",
        component: SearchPageComponent,
        componentProps: {},
      })
      .then((modelEl) => {
        modelEl.present()
        console.log('model open hua')
        modelEl.onWillDismiss().then((value:any)=>{
          console.log(value.data) 
          let data = value.data
          this.saveLocation(data.address, data.lat, data.lng);
        })
      });
    
  }

  saveLocation(subTitle, lat, lng) {
    const alert = this.alertCtrl
      .create({
        header: "Location",
        subHeader: subTitle,
        buttons: [
          {
            text: "cancel",
            role: "cancel",
          },
          {
            text: "save",
            handler: () => {
              this.modalCtrl.dismiss({ address:subTitle, lat: lat, lng: lng});
              let message = lat.toString() + "," + lng.toString();
              // this.sendMessageToFirebase(message, 'map');
              // this.locationSend(message);
              this.navCtrl.pop();
            },
          },
        ],
      })
      .then((alert) => {
        alert.present();
      });
    // alert.present();
  }

  // locationSend(message) {
  //   console.log();
  //   let data = {
  //     message: "",
  //     group_id: this.paramsData.data.id,
  //     sender_id: this.userUid,
  //     time: this.panelService.getSecond(),
  //     toUserId: this.paramsData.data.uid,
  //     type: "location",
  //     location: message,
  //     g_name: this.paramsData.data.name,
  //     sender_name:
  //       this.userService.getUsersData().first_name +
  //       " " +
  //       this.userService.getUsersData().last_name,
  //   };
  //   console.log(message);
  //   // const loading = this.loadingCtrl.create({
  //   //   content: 'Please wait'
  //   // });
  //   // loading.present();
  //   // this.panelService.insertMessage(data)
  //   //       .subscribe((success)=>{
  //   //         // loading.dismiss();
  //   //         console.log(success);
  //   //         // this.mediaData = success.data;
  //   //         this.panelService.sendMedia(data);
  //   //       },(err)=>{
  //   //         loading.dismiss();
  //   //         console.log(err);

  //   //       });
  // }

  private alert(title, subTitle) {
    const alert = this.alertCtrl
      .create({
        header: title,
        subHeader: subTitle,
        buttons: [
          {
            text: "Ok",
            role: "cancel",
          },
        ],
      })
      .then((alert) => {
        alert.present();
      });
  }

  private toast(message) {
    this.panelService.successToast(message);
  }

  getTime() {
    return new Date().getTime().toString();
  }


}

