import { Component, OnInit ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-manual-adress',
  templateUrl: './manual-adress.component.html',
  styleUrls: ['./manual-adress.component.scss'],
})
export class ManualAdressComponent implements OnInit {

  constructor(private router:Router,private modalCtrl:ModalController) { }

  ngOnInit() {}

  openMyCart(){
    this.modalCtrl.dismiss()
    this.router.navigateByUrl('my-cart')
  }

}
