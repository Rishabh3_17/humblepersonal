import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DaywiseSubscriptionPageRoutingModule } from './daywise-subscription-routing.module';

import { DaywiseSubscriptionPage } from './daywise-subscription.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DaywiseSubscriptionPageRoutingModule
  ],
  declarations: [DaywiseSubscriptionPage]
})
export class DaywiseSubscriptionPageModule {}
