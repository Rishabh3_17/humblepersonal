import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DaywiseSubscriptionPage } from './daywise-subscription.page';

const routes: Routes = [
  {
    path: '',
    component: DaywiseSubscriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DaywiseSubscriptionPageRoutingModule {}
