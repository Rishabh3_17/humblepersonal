import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-check-dets',
  templateUrl: './check-dets.page.html',
  styleUrls: ['./check-dets.page.scss'],
})
export class CheckDetsPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }


  cancelPayment(){
    this.router.navigateByUrl('main-page/home/wallet')
  }

  submitPayment(){
    this.router.navigateByUrl('main-page/home')
  }
}
