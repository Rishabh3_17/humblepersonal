import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckDetsPage } from './check-dets.page';

describe('CheckDetsPage', () => {
  let component: CheckDetsPage;
  let fixture: ComponentFixture<CheckDetsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckDetsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckDetsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
