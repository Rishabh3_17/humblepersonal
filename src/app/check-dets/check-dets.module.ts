import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckDetsPageRoutingModule } from './check-dets-routing.module';

import { CheckDetsPage } from './check-dets.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckDetsPageRoutingModule
  ],
  declarations: [CheckDetsPage]
})
export class CheckDetsPageModule {}
