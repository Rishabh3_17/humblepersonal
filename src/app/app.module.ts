import { Geolocation } from "@ionic-native/geolocation/ngx";
import { ComponentsModule } from "./components/components.module";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import {
  RoundProgressModule,
  ROUND_PROGRESS_DEFAULTS,
} from "angular-svg-round-progressbar";
import { CalendarModule, MonthComponent } from "ion2-calendar";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { SignInWithApple } from "@ionic-native/sign-in-with-apple/ngx";
import { Camera } from '@ionic-native/camera/ngx'
import { Network } from "@ionic-native/network/ngx";
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    FormsModule,
    HttpClientModule,
    CalendarModule,
    RoundProgressModule,
    ComponentsModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    DatePipe,
    CalendarModule,
    MonthComponent,
    Geolocation,
    StatusBar,
    GooglePlus,
    SplashScreen,
    SignInWithApple,
    Camera,
    Network,
    {
      provide: { CalendarModule, ROUND_PROGRESS_DEFAULTS, RouteReuseStrategy },
      useClass: IonicRouteStrategy,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
