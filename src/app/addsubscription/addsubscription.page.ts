import { SubscribeComponent } from './../components/subscribe/subscribe.component';
import { Component,OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addsubscription',
  templateUrl: './addsubscription.page.html',
  styleUrls: ['./addsubscription.page.scss'],
})
export class AddsubscriptionPage implements OnInit {
valueofRadio : any;
data:any;
radioValue="EveryDay";
  constructor(private router: Router,private modalCtrl:ModalController) { }

  ngOnInit() {
    
    console.log(this.radioValue);
  }

  gotoSubscribe(){  
    this.modalCtrl.create({
      cssClass:'subscribeModal',
      component:SubscribeComponent,
      componentProps:{

      }
    }).then((modelEl)=>{
      modelEl.present();
    })
  }
  gotomyCart(){
    this.router.navigateByUrl('my-cart')
  }

  setIcon(){
    console.log(this.radioValue)
  }
}
