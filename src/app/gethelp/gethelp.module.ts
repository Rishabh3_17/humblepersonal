import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GethelpPageRoutingModule } from './gethelp-routing.module';

import { GethelpPage } from './gethelp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GethelpPageRoutingModule
  ],
  declarations: [GethelpPage]
})
export class GethelpPageModule {}
