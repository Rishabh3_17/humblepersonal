import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GethelpPage } from './gethelp.page';

const routes: Routes = [
  {
    path: '',
    component: GethelpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GethelpPageRoutingModule {}
