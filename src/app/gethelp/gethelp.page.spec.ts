import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GethelpPage } from './gethelp.page';

describe('GethelpPage', () => {
  let component: GethelpPage;
  let fixture: ComponentFixture<GethelpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GethelpPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GethelpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
