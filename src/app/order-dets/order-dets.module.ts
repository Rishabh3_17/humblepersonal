import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderDetsPageRoutingModule } from './order-dets-routing.module';

import { OrderDetsPage } from './order-dets.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderDetsPageRoutingModule
  ],
  declarations: [OrderDetsPage]
})
export class OrderDetsPageModule {}
