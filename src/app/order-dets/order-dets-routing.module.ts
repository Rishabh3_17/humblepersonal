import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderDetsPage } from './order-dets.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDetsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderDetsPageRoutingModule {}
