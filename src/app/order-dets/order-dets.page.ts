import { PanelService } from './../services/panel.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order-dets',
  templateUrl: './order-dets.page.html',
  styleUrls: ['./order-dets.page.scss'],
})
export class OrderDetsPage implements OnInit {
id:any;
orderDetail:any;
  constructor(private router : Router,private activatedRoute:ActivatedRoute,private panelService:PanelService) { 
    this.activatedRoute.queryParams.subscribe(data=>{
      this.id = data.id
      this.getOrderDetail() 
      console.log(this.id)
    })
  }

  ngOnInit() {
  }

  private tutorialHidden: boolean = true;

  abrirTutorial(){

    if(this.tutorialHidden === true){

      this.tutorialHidden = false;
      document.getElementById("tutorial").hidden = false;

    }else if(this.tutorialHidden === false){

      this.tutorialHidden = true;
      document.getElementById("tutorial").hidden = true;

    }

  }
  gotoHelp(){
    this.router.navigateByUrl('gethelp')
  }
  getOrderDetail(){
    this.panelService.getOrderDetail(this.id).subscribe((res:any)=>{
      console.log(res)
      this.orderDetail = res.data
    },(err)=>{
      console.log(err)
    })
  }
}

