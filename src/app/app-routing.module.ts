import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'main-page',
    loadChildren: () => import('./main-page/main-page.module').then( m => m.MainPagePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule),
    canActivate:[AuthGuard]
  },
  {
    path: 'myvacation',
    loadChildren: () => import('./myvacation/myvacation.module').then( m => m.MyvacationPageModule)
  },
  {
    path: 'subscriptiondets',
    loadChildren: () => import('./subscriptiondets/subscriptiondets.module').then( m => m.SubscriptiondetsPageModule)
  },
  {
    path: 'daywise-subscription',
    loadChildren: () => import('./daywise-subscription/daywise-subscription.module').then( m => m.DaywiseSubscriptionPageModule)
  },
  {
    path: 'order-dets',
    loadChildren: () => import('./order-dets/order-dets.module').then( m => m.OrderDetsPageModule)
  },
  {
    path: 'gethelp',
    loadChildren: () => import('./gethelp/gethelp.module').then( m => m.GethelpPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'my-cart',
    loadChildren: () => import('./my-cart/my-cart.module').then( m => m.MyCartPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./checkout/checkout.module').then( m => m.CheckoutPageModule)
  },
  {
    path: 'addsubscription',
    loadChildren: () => import('./addsubscription/addsubscription.module').then( m => m.AddsubscriptionPageModule)
  },
  {
    path: 'map-location',
    loadChildren: () => import('./map-location/map-location.module').then( m => m.MapLocationPageModule)
  },
  {
    path: 'check-dets',
    loadChildren: () => import('./check-dets/check-dets.module').then( m => m.CheckDetsPageModule)
  },
  {
    path: 'bank-dets',
    loadChildren: () => import('./bank-dets/bank-dets.module').then( m => m.BankDetsPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'edit-page',
    loadChildren: () => import('./edit-page/edit-page.module').then( m => m.EditPagePageModule)
  },
  {
    path: 'filter',
    loadChildren: () => import('./filter/filter.module').then( m => m.FilterPageModule)
  },
  {
    path: 'subscribe-dets',
    loadChildren: () => import('./subscribe-dets/subscribe-dets.module').then( m => m.SubscribeDetsPageModule)
  },
  {
    path: 'amount-to-bank',
    loadChildren: () => import('./amount-to-bank/amount-to-bank.module').then( m => m.AmountToBankPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
