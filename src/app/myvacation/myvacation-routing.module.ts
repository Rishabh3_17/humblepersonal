import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyvacationPage } from './myvacation.page';

const routes: Routes = [
  {
    path: '',
    component: MyvacationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyvacationPageRoutingModule {}
