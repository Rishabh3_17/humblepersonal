import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyvacationPage } from './myvacation.page';

describe('MyvacationPage', () => {
  let component: MyvacationPage;
  let fixture: ComponentFixture<MyvacationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyvacationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyvacationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
