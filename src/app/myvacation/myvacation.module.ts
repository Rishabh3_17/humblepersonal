import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyvacationPageRoutingModule } from './myvacation-routing.module';

import { MyvacationPage } from './myvacation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyvacationPageRoutingModule
  ],
  declarations: [MyvacationPage]
})
export class MyvacationPageModule {}
