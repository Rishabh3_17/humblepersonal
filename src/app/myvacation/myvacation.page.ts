import { PanelService } from 'src/app/services/panel.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { VacationComponent } from '../components/vacation/vacation.component';

@Component({
  selector: 'app-myvacation',
  templateUrl: './myvacation.page.html',
  styleUrls: ['./myvacation.page.scss'],
})
export class MyvacationPage implements OnInit {

  myVacations;
  page;

  constructor(private modalCtrl: ModalController,private panelService:PanelService) { 
    // this.showVacation()
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.showVacation();
  }

  showVacation(ev?, type?) {
    if (type === "infinite") {
      this.page++;
    } else {
      this.page = 1;
    }
    this.panelService.showVacation(this.page).subscribe((res:any) => {
      console.log("My Vacations");
      console.log(res);
      if (ev) {
        ev.target.complete();
      }
      this.myVacations = (type === "infinite") ? [...this.myVacations, ...res.total_vocation.data] : res.total_vocation.data;
      console.log(this.myVacations);
    },(err:any) => {
      console.log(err);
      if (ev) {
        ev.target.complete();
      }
    });
  }

  openVacationModal(){  
    this.modalCtrl.create({
      cssClass:'vacationModal',
      component:VacationComponent,
      componentProps:{

      }
    }).then((ModelEl)=>{
      ModelEl.present();
    })
  }

  doInfinite(ev) {
    this.showVacation(ev, "infinite");
  }
}
