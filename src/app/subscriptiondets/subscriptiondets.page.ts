import { PanelService } from 'src/app/services/panel.service';
import { CssSelector } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-subscriptiondets',
  templateUrl: './subscriptiondets.page.html',
  styleUrls: ['./subscriptiondets.page.scss'],
})
export class SubscriptiondetsPage implements OnInit {
  current= 20;
  max= 50;
  id:any;
  orderDetail:any;
  // color:'linear-gradient(180deg,#EFA7BF,#BD7299)'
  constructor(private router : Router,private activatedRoute:ActivatedRoute,private panelService:PanelService) { 
    this.activatedRoute.queryParams.subscribe(data=>{
      this.id = data.id 
      console.log(this.id)
      this.getOrderDetail()
    })
  }

  ngOnInit() {
  }

  private tutorialHidden: boolean = true;

  abrirTutorial(){

    if(this.tutorialHidden === true){

      this.tutorialHidden = false;
      document.getElementById("tutorial").hidden = false;

    }else if(this.tutorialHidden === false){

      this.tutorialHidden = true;
      document.getElementById("tutorial").hidden = true;
    }

  }

  gotodayWiseSubscription(){
    this.router.navigateByUrl('daywise-subscription')
  }

  getOrderDetail(){
    this.panelService.getOrderDetail(this.id).subscribe((res:any)=>{
      console.log(res)
      this.orderDetail = res.data
    },(err)=>{
      console.log(err)
    })
  }

}

