import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubscriptiondetsPage } from './subscriptiondets.page';

const routes: Routes = [
  {
    path: '',
    component: SubscriptiondetsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscriptiondetsPageRoutingModule {}
