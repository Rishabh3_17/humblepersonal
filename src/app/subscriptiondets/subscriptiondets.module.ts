import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubscriptiondetsPageRoutingModule } from './subscriptiondets-routing.module';

import { SubscriptiondetsPage } from './subscriptiondets.page';
import { RoundProgressModule  } from 'angular-svg-round-progressbar';

@NgModule({
  imports: [
    RoundProgressModule,
    CommonModule,
    FormsModule,
    IonicModule,
    SubscriptiondetsPageRoutingModule
  ],
  declarations: [SubscriptiondetsPage]
})
export class SubscriptiondetsPageModule {}
