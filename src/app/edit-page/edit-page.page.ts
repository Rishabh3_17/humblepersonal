import { Router } from '@angular/router';
import { UserService } from './../services/user.service';
import { PanelService } from 'src/app/services/panel.service';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.page.html',
  styleUrls: ['./edit-page.page.scss'],
})
export class EditPagePage implements OnInit {
  formData = {
    first_name:'',
    last_name:'',
    gender:'',
    dob:''
  }
  form:FormGroup    

  constructor(private panelService:PanelService,private userService:UserService,private router:Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      first_name:this.panelService.validatorsFunction(),
      last_name:this.panelService.validatorsFunction(),
      dob:this.panelService.validatorsFunction(),
      gender:this.panelService.validatorsFunction()
    })
  }

  logForm(f) {
    if(this.form.valid) {
      let dob = moment(this.formData.dob).format("MM/DD/YYYY");
      let data  = new FormData;
      data.append('first_name',this.formData.first_name);
      data.append('last_name',this.formData.last_name);
      data.append('gender',this.formData.gender);
      data.append('dob',dob);
      this.userService.editProfile(data).subscribe((res:any) => {
        console.log(res.user);
        this.userService.saveUsersData(res.user);
        this.router.navigateByUrl('/main-page/myaccount?reload=true');
      }, (err:any) => {
        console.log(err);
      });
      console.log(this.formData.dob);
    } 
    else {
      this.form.markAllAsTouched();
      console.log('not done');
    }
  }
}
