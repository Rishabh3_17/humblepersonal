import { UserService } from './user.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URLS } from './urls.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient,private userService:UserService) { }
 
  userSignUp(formData){
   return this.http.post(URLS.baseUrl+'/sign-up',formData)
  }

  userSignin(inputData){
    console.log(inputData)
    // console.log(obj)
    return this.http.post(URLS.baseUrl+'/sign-in',inputData)
  }
  
  socialLogin(data){
    return this.http.post(URLS.baseUrl+'/socio-login',data)
  }

  otpVerify(otpVerifyData){
    return this.http.post(URLS.baseUrl+'/otp-verify',otpVerifyData)
  }
  reSendOtp(otpResend,type){
    console.log()
    return this.http.post(URLS.baseUrl+'/otp-resend/'+type,otpResend)
  }

  userLogout(){
    this.userService.userLogout();
  }

  get userIsAuthenticated(){
    return this.userService.authenticationState.value
  }
}

