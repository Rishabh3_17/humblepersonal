import { PanelService } from "./panel.service";
import { UserService } from "./user.service";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { URLS } from "./urls.service";
import * as moment from "moment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CartService {
  cartLocalStorageName = "carts";
  cartData = {
    items: [],
    total_price: 0,
    total_qty: 0,
  };
  productData: productData = {};
  getCartData: any;
  data: any;
  getcartData = {
    items: {
      one_time_orders: [],
      subscription_orders: [],
    },
    total_amount: 0,
    total_qty: 0,
  };
  observable: any;
  showCartItemPoppup: any;
  dataInCart:any;
  constructor(
    private userService: UserService,
    private http: HttpClient,
    private panelService: PanelService
  ) {
    console.log("date = " + moment().format("MMM Do YY"));
    // this.getCart();
    console.log("cart data after cart " + this.cartData.items);
  }

  // async getCart() {
  //   this.dataInCart = false;
  //   console.log(this.userService.authenticationState);
  //   if (this.userService.authenticationState === "true") {
  //     this.getcartData = {
  //       items: {
  //         one_time_orders: [],
  //         subscription_orders: [],
  //       },
  //       total_amount: 0,
  //       total_qty: 0,
  //     };
  //     this.getCartFromApi().subscribe(
  //       (res: any) => {
  //         console.log(res);
  //         this.getcartData.items.one_time_orders = res.one_time_order;
  //         this.getcartData.items.subscription_orders = res.subscription_order;
  //         this.getcartData.total_amount = res.total_amount;
  //         this.getcartData.total_qty = res.total_item;
  //         this.dataInCart = true
  //         console.log(this.getcartData);
  //       },
  //       (err: any) => {
  //         console.log(err);
  //       }
  //     );
  //   } else {
  //     let cart = this.getCartFromLocal();
  //     this.getcartData = {
  //       items: {
  //         one_time_orders: [],
  //         subscription_orders: [],
  //       },
  //       total_amount: 0,
  //       total_qty: 0,
  //     };
  //     this.findproductinlocal();
  //   }
  //   return this.getcartData;
  // }

  // observablefunction() {
  //   console.log(this.getCartData);
  //   let observable = new Observable((cartdata) => {
  //     cartdata.next(this.getcartData);
  //   });
  //   return observable;
  // }

  // findproductinlocal() {
  //   console.log(this.getcartData);
  //   this.getcartData.total_amount = this.cartData.total_price;
  //   this.getcartData.total_qty = this.cartData.total_qty;
  //   for (let i = 0; i < this.cartData.items.length; i++) {
  //     if (this.cartData.items[i].type === "o") {
  //       console.log("one time mila");
  //       this.getcartData.items.one_time_orders.push(this.cartData.items[i]);
  //       console.log(this.getcartData);
  //     } else if (this.cartData.items[i].type === "s") {
  //       console.log("subscription mila");
  //       this.getcartData.items.subscription_orders.push(this.cartData.items[i]);
  //       console.log(this.getcartData);
  //     }
  //   }
  //   console.log(this.getcartData);
  // }

  getCartFromApi() {
    return this.http.get(URLS.baseUrl + "/orders/cart/show", {
      headers: this.userService.header(),
    });
  }

  // getCartFromLocal() {
  //   const str = localStorage.getItem(this.cartLocalStorageName);
  //   if (str) {
  //     const oldCart = JSON.parse(str);
  //     this.cartData = oldCart;
  //     console.log(this.cartData);
  //   }
  //   return this.cartData;
  // }

  // saveCart(product) {
  //   if (this.userService.authenticationState === "true") {
  //     this.saveCartToApi(product);
  //   } else {
  //     this.saveCartToLocal();
  //   }
  // }

  // saveCartToApi(product) {
  //   console.log(product);
  //   this.addItemWhenLogin(product)
    
  // }

  // saveCartToLocal() {
  //   const str = JSON.stringify(this.cartData);
  //   if (str) {
  //     localStorage.setItem(this.cartLocalStorageName, str);
  //     this.getCart();
  //   }
  // }

  // addItem(product) {
  //   // product.id = this.generateId();
  //   this.cartData.items.push(product);
  //   this.cartData.total_qty += +product.quantity;
  //   this.cartData.total_price += +product.price;
  //   console.log(this.cartData);
  //   this.saveCart(product);
  //   return "success";
  //   // this.showToast('Cart update successfully');
  // }

  // addQuantityItem(data, productType) {
  //   console.log("kuch to ho ");
  //   console.log(data);
  //   const product = this.cartData.items.find(
  //     (d) => d.product_id === data.id && d.type === productType
  //   );
  //   if (productType === "o") {
  //     product.quantity = data.qty;
  //   } else if (productType === "s") {
  //     product.id = data.id;
  //     product.quantity = data.qty;
  //     product.start_date = data.start_date;
  //     (product.end_date = data.end_date), (product.week_day = data.week_day);
  //   }
  //   product.price = +product.basic_price * product.quantity;
  //   this.cartData.total_qty++;
  //   this.cartData.total_price += +product.basic_price;
  //   console.log(this.cartData);
  //   this.saveCartToLocal();
  //   console.log("added");
  //   return;
  // }

  // findProductInCart(product, productType) {
  //   let productFound = this.cartData.items.find(
  //     (d) => d.product_id === product.id && d.type === productType
  //   );
  //   console.log(productFound);
  //   return productFound;
  // }

  // subtractQuantityItem(id, productType) {
  //   const product = this.cartData.items.find(
  //     (d) => d.product_id === id && d.type === productType
  //   );
  //   if (product.quantity <= 1) {
  //     return;
  //   }
  //   product.quantity = +product.quantity - 1;
  //   product.price = +product.basic_price * product.quantity;
  //   this.cartData.total_qty--;
  //   this.cartData.total_price -= +product.basic_price;
  //   console.log(this.cartData);
  //   this.saveCartToLocal();
  //   console.log("added");
  // }

  // addProductByProductType(
  //   product,
  //   checked,
  //   start_date,
  //   end_date,
  //   productType,
  //   delivery_date,
  //   count,
  //   delivery_shift,
  //   productTypeId
  // ) {
  //   console.log("date:" + delivery_date);
  //   console.log("producttype" + productType);
  //   if (this.userService.authenticationState === "true") {
  //     this.addProductWhenUserisLogin(
  //       product,
  //       checked,
  //       start_date,
  //       end_date,
  //       productType,
  //       delivery_date,
  //       count,
  //       delivery_shift,
  //       productTypeId
  //     );
  //     // user is login and data saved via api
  //   } else {
  //     console.log("cartData:" + this.cartData);
  //     this.addProductInLocalStorage(
  //       product,
  //       checked,
  //       start_date,
  //       end_date,
  //       productType,
  //       delivery_date,
  //       count,
  //       delivery_shift,
  //       productTypeId
  //     );
  //   }
  //   return "Success";
  // }

  addProductWhenUserisLogin(
    product,
    checked,
    start_date,
    end_date,
    productType,
    delivery_date,
    count,
    delivery_shift,
    productTypeId
  ) {
    console.log(count);
    this.productData = {
      product_id: product.id,
      product_type_id: productTypeId,
      quantity: count,
      total_amount: product.sell_price,
      type: productType,
      delivery_date: delivery_date,
      end_date: end_date,
      start_date: start_date,
      delivery_shift: delivery_shift,
      week_day: checked,
    };

   return this.http.post(URLS.baseUrl + "/orders/cart/item/add", this.productData, {
      headers: this.userService.header(),
    })
    
  }

  // addProductInLocalStorage(
  //   product,
  //   checked,
  //   start_date,
  //   end_date,
  //   productType,
  //   delivery_date,
  //   count,
  //   delivery_shift,
  //   productTypeId
  // ) {
  //   if (productType === "o") {
  //     if (this.findProductInCart(product, productType)) {
  //       this.addQuantityItem(product, productType);
  //       console.log("done");
  //       return "cart Updated success";
  //     } else {
  //       this.productData = {
  //         products: {
  //           name: product.name,
  //         },
  //         amount: product.sell_price,
  //         product_id: product.id,
  //         product_type_id:productTypeId,
  //         quantity: count,
  //         basic_price: product.price,
  //         price: product.price,
  //         type: productType,
  //         delivery_date: delivery_date,
  //         delivery_shift: delivery_shift,
  //       };
  //     }
  //   } else if (productType === "s") {
  //     console.log("pahoch gya");
  //     if (this.findProductInCart(product, productType)) {
  //       this.addQuantityItem(product, productType);
  //       console.log("cart me add hogya");
  //       return "cart Updated success";
  //     } else {
  //       // const mapData = product.productDatas.find((d) => d.map_ing_data_id == product.Id);
  //       // console.log(mapData);
  //       console.log(count);
  //       this.productData = {
  //         products: {
  //           name: product.name,
  //         },
  //         amount: product.sell_price,
  //         product_id: product.id,
  //         product_type_id:productTypeId,
  //         // map_ing_data_id: mapData.map_ing_data_id,
  //         start_date: start_date,
  //         end_date: end_date,
  //         quantity: count,
  //         // basic_price: product.basic_price,
  //         price: product.basic_price,
  //         type: productType,
  //         delivery_shift: delivery_shift,
  //         week_day: checked
  //       };
  //     }
  //   }
  //   this.addItem(this.productData);
  // }

  // clear cart data

  clearCartData() {
    this.cartData = {
      items: [],
      total_price: 0,
      total_qty: 0,
    };
    console.log(this.cartData);
    localStorage.removeItem(this.cartLocalStorageName);
  }

  // when user is login
  addItemWhenLogin(productData) {
    this.http.post(URLS.baseUrl + "/orders/cart/item/add", productData, {
      headers: this.userService.header(),
    }).subscribe((res)=>{ 
      // this.getApiresponse(res).subscribe((ress:any)=>{
      //   console.log(ress)
      //   // this.getCart()
      // })
      console.log(res)
    },(err:any)=>{
      console.log(err)   
      // this.getCart() 
      // this.getApiresponse(err).subscribe((errr:any)=>{
      //   console.log(errr)
      // });
    })    
  }

  // getApiresponse(res){      
  //     let observable = new Observable((observe) => {
  //           observe.next(res);
  //       // if(from === 'fromservice'){
  //       //   console.log("service se call hua h ")
  //       //   observe.next(res);
  //       // }else if(from === 'fromts'){
  //       //   console.log("ts se call hua h ")
  //       // }else{
  //       //   console.log("chal hi nhi rha h")
  //       // }      
  //     });
  //     return observable;
    
  // }


  
  // remove count
  removeone(count) {
    count -= 1;
    return count;
  }

  // add count
  addone(count) {
    count += 1;
    return count;
  }

  // clear cart

  clearCart() {
    return this.http
        .get(URLS.baseUrl + "/orders/cart/clear", {
          headers: this.userService.header(),
        })
        
    // if (this.userService.userIsAuthenticated()) {
      
    // } else {
    //   this.clearCartData();
    // }
     // this.getCart();
  }

  // reove one   item from cart

  removeItemFromCart(id) {
    let data = {
      id: id,
    };

    return this.http
        .post(URLS.baseUrl + "/orders/cart/item/delete", data, {
          headers: this.userService.header(),
        })
  }



  // remove one item from local

  // subtractItem(id, productType) {
  //   const product = this.cartData.items.find(
  //     (d) => d.id === id && d.type === productType
  //   );
  //   const productIndex = this.cartData.items.findIndex(
  //     (d) => d.id === id && d.type === productType
  //   );
  //   this.cartData.total_price -= +product.price;
  //   this.cartData.total_qty -= +product.quantity;
  //   this.cartData.items.splice(productIndex, 1);
  //   console.log(this.cartData);
  //   this.saveCartToLocal();
  //   // this.showToast('Cart update successfully');
  // }

  // observable for api response

  catchForOnline() {
    let observable = new Observable((observe) => {
      observe.next(this.data);
    });
    return observable;
  }

  // edit cart item

  // updateQuantity(data, productType) {
  //   if (this.userService.authenticationState === "true") {
  //     console.log(data);
  //     this.editCartviaapi(data).subscribe(
  //       (res: any) => {
  //         console.log(res);
  //       },
  //       (err: any) => {
  //         console.log(err);
  //       }
  //     );
  //     // user is login and data saved via api
  //   } else {
  //     // this.addQuantityItem(data, productType);
  //   }
  // }

  //  edit cart via api
  editCartviaapi(data) {
    return this.http.post(URLS.baseUrl + "/orders/cart/item/edit", data, {
      headers: this.userService.header(),
    });
  }
  // main page cart show hide

  cartcheckdata() {
    // console.log('yha se cart service ka get cart data')
    // console.log( this.getCartData)
    let observable = new Observable((cartPopup) => {
      if (this.getCartData.total_item === 0) {
        cartPopup.next(false);
      } else if (this.getCartData.total_item !== 0) {
        cartPopup.next(true);
      }
    });
    return observable;
  }
}


// interface for add product
export interface productData {
  products?;
  amount?;
  product_id?;
  product_type_id?;
  quantity?;
  total_amount?;
  type?;
  week_day?;
  delivery_date?;
  end_date?;
  start_date?;
  price?;
  basic_price?;
  delivery_shift?;
}
