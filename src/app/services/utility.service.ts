import { PanelService } from './panel.service';
import { Network } from '@ionic-native/network/ngx';
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})

export class UtilityService {
    constructor(
        private panelService: PanelService,
        private network: Network
    ) {}

    netConnection() {
        // this.network.onConnect().subscribe( () => {
        //     console.log("Connected to network");
        //     this.panelService.successToast("Connected to network");
        // });
        this.network.onDisconnect().subscribe( () => {
            console.log("Not Conncted to any Network");
            this.panelService.errorToast("No Internet");
        });
    }
}