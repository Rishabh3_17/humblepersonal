import { UserService } from './user.service';
import { URLS } from './urls.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { FormControl, Validators } from '@angular/forms';
import { UrlSegment } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PanelService {
  userData: any;
  constructor(private http: HttpClient, private toastController: ToastController, private loadingController: LoadingController, private userService: UserService) {

    this.userData = this.userService.getUsersData()
  }


  


  // location 
  getTime() {
    return new Date().getTime();
  }

  getSecond() {
    return Math.floor(this.getTime() / 1000);
  }



  async successToast(success) {
    const toast = await this.toastController.create({
      message: success,
      duration: 2000,
      color: 'primary'
    });
    toast.present();
  }

  async errorToast(error) {
    // console.log(error)
    const toast = await this.toastController.create({
      message: error,
      duration: 2000,
      color: 'medium'
    });
    toast.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      // cssClass: 'my-custom-class',
      // message: 'Please wait...',
      // duration: 2000
    });
    await loading.present();
    // const { role, data } = await loading.onDidDismiss();
    // console.log('Loading dismissed!');
  }
  
  dismissLoading() {
    this.loadingController.dismiss()
  }

  validatorsFunction() {
    return new FormControl('', {
      updateOn: 'submit',
      validators: [
        Validators.required
      ]
    })
  }
  emailvalidatorsFunction() {
    return new FormControl('', {
      updateOn: 'submit',
      validators: [
        Validators.required,
        Validators.email
      ],

    })
  }

  getSubCategories(id, page) {
    return this.http.get(`${URLS.baseUrl}/categories/${id}/sub-category?page=${page}`);
    // return this.http.get(URLS.baseUrl + '/categories/' + id + '/sub-category');
  }

  getProduct(item, page) {
    return this.http.get(`${URLS.baseUrl}/categories/${item}/get-product?page=${page}`);
    // return this.http.get(URLS.baseUrl + '/categories/' + item + '/get-product')
  }

  getOrders(orderType, url) {
    console.log("accesss");
    return this.http.get(URLS.baseUrl + '/orders/' + orderType + '/' + url, { headers: this.userService.header() })

  }

  categoriesApi(page?) {
    //  return this.http.get(URLS.baseUrl+'/categories');  
    return this.http.get(`${URLS.baseUrl}/categories?page=${page}`);
  }
  getSubscriptionOnParticularDate(startDate) {
    let data = {
      subscription_date: startDate
    }
    return this.http.post(URLS.baseUrl + '/orders/subscription/show', data, { headers: this.userService.header() })
  }

  getWalletBalance() {
    return this.http.get(URLS.baseUrl + '/wallet/balance', { headers: this.userService.header() })
  }

  getTransaction(status, page) {
    return this.http.get(`${URLS.baseUrl}/wallet/transaction?page=${page}&payment_status=${status}`, { headers: this.userService.header() });
    // return this.http.get(URLS.baseUrl + '/wallet/transaction', { headers: this.userService.header() });
  }

  // hello(){
  //   var myInput = 123
  //   this.searchProducts(myInput).subscribe((res)=>{
  //     // response of api
  //     console.log(res)
  //   },(err)=>{
  //     // error handeling
  //     console.log(err)
  //   })
  // }

  searchProducts(myInput) {
    return this.http.get(URLS.baseUrl + `/products/search?name=${myInput}`)
  }

  applyFilter(data, page?) {
   return  this.http.post(URLS.baseUrl + '/products/filter', data)
  }

  getProductById(data) {
    console.log(data)
    return this.http.post(URLS.baseUrl + '/products/get', data)
  }

  takeVacation(data) {
    return this.http.post(URLS.baseUrl + '/orders/subscription/vocation/take', data, { headers: this.userService.header() })
  }

  showVacation(page) {
    return this.http.get(`${URLS.baseUrl}/orders/subscription/vocation/show?page=${page}`, { headers: this.userService.header() })
    // return this.http.get(URLS.baseUrl + '/orders/subscription/vocation/show', { headers: this.userService.header() })
  }


  payment(data) {
    return console.log('payment')
  }

  getUserData() {
    return this.http.get(`${URLS.baseUrl}/my-profile`, { headers: this.userService.header() });
  }

  razorPaySuccessPayment(data) {
    return this.http.post(URLS.baseUrl + '/wallet/add-amount', data, { headers: this.userService.header() })
  }

  getEssentials() {
    return this.http.get(URLS.baseUrl + '/products/essential')
  }

  getBestSellers() {
    return this.http.get(URLS.baseUrl + '/products/best-seller')
  }

  getAddress() {
    return this.http.get(URLS.baseUrl + '/orders/show-delivery-address', { headers: this.userService.header() })
  }
  addAddress(data) {
    return this.http.post(URLS.baseUrl + '/orders/add-delivery-address', data, { headers: this.userService.header() })
  }

  sendMoneyToBank(data) {
    return this.http.post(URLS.baseUrl + '/wallet/amount-transfer', data, { headers: this.userService.header() })
  }

  checkOut(data) {
    return this.http.post(URLS.baseUrl + '/orders/check-out', data, { headers: this.userService.header() })
  }

  getTransferToBankHistory(status, page) {
    return this.http.get(`${URLS.baseUrl}/transferTobank/history?page=${page}&payment_status=${status}`, { headers: this.userService.header() });
  }

  uploadProfilePicture(image) {
    console.log(image);
    let profilePicture = new FormData();
    profilePicture.append("profile", image);
    return this.http.post(`${URLS.baseUrl}/add-profile`, profilePicture, { headers: this.userService.header() })
  }

  // this will convert your base64 to file type
  dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    }
    else {
      byteString = encodeURI(dataURI.split(',')[1]);
    }
    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: mimeString });
  }

  getOrderDetail(id){
    return this.http.get(URLS.baseUrl+'/orders/details/'+id,{headers:this.userService.header()})
  }

}

