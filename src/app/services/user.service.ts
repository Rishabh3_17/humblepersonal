import { PanelService } from 'src/app/services/panel.service';
import { URLS } from './urls.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  ;
  loginDets:any;
  userDataCollection ='userDataCollection';
  userData:any;
  // public authenticationState = new BehaviorSubject(false)
  authenticationState :any;
  constructor(private http:HttpClient) {
    this.isuserislogin()
   }


   isuserislogin(){
    this.loginDets = this.getUsersData()
    if(this.loginDets === null){
       this.authenticationState = 'false'
    }
    else{
      this.authenticationState= 'true'
    }
   }


  header(){
    console.log(this.loginDets)
     return new HttpHeaders({
    'Authorization':`Bearer ${this.loginDets.api_token}`
    })
  }


  modifyAuthState(){
    this.authenticationState= 'true'
     console.log(this.authenticationState)
  }

  saveUsersData(userData){
    this.userData= userData;  
    console.log(this.userData)
    let userDets = JSON.stringify(userData)
      console.log(userData);
      localStorage.setItem(this.userDataCollection,userDets)
      this.isuserislogin()
  }

  userIsAuthenticated(){
    if(this.authenticationState === 'true'){
      return true;
    }
    else{
      return false;
    }
  }

  getUsersData(){
    let userDets = localStorage.getItem(this.userDataCollection);
    return  JSON.parse(userDets);
    
  }

  userLogout(){
    console.log(this.userDataCollection);
    // this.authenticationState.next(false);
    this.authenticationState= 'false'
    console.log(this.authenticationState.value);
    localStorage.removeItem(this.userDataCollection);
  }

  forgotPass(forgotData){
    return this.http.post(URLS.baseUrl+'/forgot-password',forgotData)
  }

  resetPassword(newPassword){
    return this.http.post(URLS.baseUrl+'/verify-forgot-otp',newPassword)
  }

  editProfile(data){
    return this.http.post(URLS.baseUrl+'/edit',data,{
      headers:this.header()
    })
  }
}



