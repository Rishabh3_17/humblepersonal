import { FormGroup } from '@angular/forms';
import { VerifyotpComponent } from './../components/verifyotp/verifyotp.component';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { PanelService } from '../services/panel.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  formData = {
    first_name:'',
    last_name:'',
    email:'',
    mobile:'',
    // city:'',
    password:'',
    password_confirmation:''
  }
  form:FormGroup;


  constructor(private router:Router,private authService:AuthService,private ModalCtrl:ModalController,private panelService:PanelService) { }

  ngOnInit() {
    this.form = new FormGroup({
      first_name:this.panelService.validatorsFunction(),
      last_name:this.panelService.validatorsFunction(),
      email:this.panelService.emailvalidatorsFunction(),
      mobile:this.panelService.validatorsFunction(),
      password:this.panelService.validatorsFunction(),
      password_confirmation:this.panelService.validatorsFunction()
    })
  }

  
 async gotoVerifyOtp(){
    
  }

  

  logForm(f){
    if(this.form.valid){
      console.log(this.formData)
      this.authService.userSignUp(this.formData).subscribe((res:any)=>{
        console.log(res)
        
        // this.panelService.presentLoading();
        this.ModalCtrl.create({
          cssClass:'verifyotp',
          component:VerifyotpComponent,
          componentProps:{ 
            email:this.formData.email       
          }
        }).then((modalEl)=>{
          modalEl.present()
        })
      },(err:any)=>{
        console.log('err')
        let errkey = err.error.errors_keys[0]
        this.panelService.errorToast(err.error.errors[errkey])
        console.log(err)
      })
    }else{
      this.form.markAllAsTouched()
    }
  }
  gotoLogin(){
    this.router.navigateByUrl('login')
  }

}

