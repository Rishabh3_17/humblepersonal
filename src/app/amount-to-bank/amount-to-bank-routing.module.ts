import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AmountToBankPage } from './amount-to-bank.page';

const routes: Routes = [
  {
    path: '',
    component: AmountToBankPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AmountToBankPageRoutingModule {}
