import { PanelService } from 'src/app/services/panel.service';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-amount-to-bank',
  templateUrl: './amount-to-bank.page.html',
  styleUrls: ['./amount-to-bank.page.scss'],
})
export class AmountToBankPage implements OnInit {
  form: FormGroup;
  transactionHistory: any = [];
  walletBalance: any;
  historyStatus = "s";
  page;

  constructor(private panelService: PanelService) { 
    this.getWalletBalance()
  }


  ngOnInit() {
    this.form = new FormGroup({
      amount: this.panelService.validatorsFunction()
    });
    this.getTransferToBankHistory();
  }
 

  getWalletBalance(){
    this.panelService.getWalletBalance().subscribe((res:any)=>{
      console.log(res)
      this.walletBalance = res.balance 
      // if(this.walletBalance <=0){
      //   // this.presentAlertConfirm()
      // }
    },(err:any)=>{

      console.log(err)
    })
  }
  logform() {
    let data = {
      amount: this.form.value.amount
    }
    // this.panelService.presentLoading()
    if (this.form.valid) {
      this.panelService.sendMoneyToBank(data).subscribe((res: any) => {
        console.log(res)
        // this.panelService.dismissLoading()
        this.panelService.successToast(res.message)
      }, (err: any) => {
        console.log(err)
        this.panelService.errorToast(err.error.message)
      })
    } else {
      // this.panelService.dismissLoading()
      this.form.markAllAsTouched()
    }
  }

  getTransferToBankHistory(status?, ev?, type?) {
    console.log(status);
    if (type === "infinite") {
      this.page++;
    } else {
      this.page = 1;
    }
    let tranStatus = (status) ? status : this.historyStatus;
    this.panelService.getTransferToBankHistory(tranStatus, this.page).subscribe((res: any) => {
      console.log(res);
      if (ev) {
        ev.target.complete();
      }
      this.transactionHistory = res.data.data;
    }, (err: any) => {
      if (ev) {
        ev.target.complete();
      }
      console.log(err);
    });
  }

  doInfinite(ev) {
    this.getTransferToBankHistory(ev, "infinite");
  }
}




