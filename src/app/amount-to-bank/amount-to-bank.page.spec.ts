import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AmountToBankPage } from './amount-to-bank.page';

describe('AmountToBankPage', () => {
  let component: AmountToBankPage;
  let fixture: ComponentFixture<AmountToBankPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmountToBankPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AmountToBankPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
