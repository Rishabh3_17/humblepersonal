import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AmountToBankPageRoutingModule } from './amount-to-bank-routing.module';

import { AmountToBankPage } from './amount-to-bank.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AmountToBankPageRoutingModule
  ],
  declarations: [AmountToBankPage]
})
export class AmountToBankPageModule {}
