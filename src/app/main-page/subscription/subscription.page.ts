import { UserService } from './../../services/user.service';
import { URLS } from './../../services/urls.service';
import { Router } from '@angular/router';
import { PanelService } from './../../services/panel.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CalendarModule ,MonthComponent} from 'ion2-calendar';
import * as moment from 'moment';
import { VacationComponent } from 'src/app/components/vacation/vacation.component';
import { DatePipe } from '@angular/common';




var startTime = new Date(Date.UTC(2014, 4, 8));
var endTime = new Date(Date.UTC(2014, 4, 9));
var endTime = new Date(Date.UTC(2014, 4, 9));

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.page.html',
  styleUrls: ['./subscription.page.scss'],
})

export class SubscriptionPage implements OnInit {
  eventSource: any;
  mode:any;
  calendars:any;
  viewTitle: string;
  Events:any;
  date: string;
  type: 'string';
  subscriptionProduct:any;
  subscriptionLength:any=0;
  imgUrl:any;
  constructor(private router:Router, private modalCtrl: ModalController,private calendarModule:CalendarModule,private monthComponent:MonthComponent,private panelService:PanelService,private datePipe:DatePipe,private userService:UserService) { 
    this.date = moment().format("ddd MMM DD YYYY")
    console.log(this.date)
    this.imgUrl = URLS.productImgUrl
  }

ngOnInit() {
  //   this.calendars=({
  //     mode:'july',
  //     currentDate:startTime
  //   })
  //   this.eventSource=({
  //     title: 'test',
  //     startTime: startTime,
  //     endTime: endTime,
  //     allDay: false
  // });
} 
ionViewDidEnter(){
  let formatDate =moment(this.date).format("MM/DD/YYYY")
  console.log(formatDate); 
  if(this.userService.userIsAuthenticated()){
    this.getSubsCription(formatDate)
  }
}

SubscriptionDets(item){
  this.router.navigate(['subscribe-dets'],{
    queryParams:item
  })
  console.log('chal gya' )
}
onChange(event) {
  let date = event._d 
  console.log(date);
  let formatDate =moment(date).format("MM/DD/YYYY")
  console.log(formatDate); 
  this.getSubsCription(formatDate)  
}
getSubsCription(date){
  this.panelService.getSubscriptionOnParticularDate(date).subscribe((res:any)=>{
    console.log(res)
    this.subscriptionProduct = res['subscription']
    console.log(this.subscriptionProduct)
    this.subscriptionLength = this.subscriptionProduct.length
    console.log(this.subscriptionLength)
  })
}

gotoHomePage(){
  this.router.navigateByUrl('main-page/home')
}


checkLogin(){
  if(this.userService.userIsAuthenticated()){
    return true
  }else{
    return false
  }
}
gotoLogin(){
  this.router.navigateByUrl('login')
}
}
