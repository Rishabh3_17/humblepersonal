import { UserService } from './../../services/user.service';
import { URLS } from './../../services/urls.service';
import { PanelService } from './../../services/panel.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
  category= "subscription";
  typeofSubscription=['OnGoing','Previous','Cancelled']
  Subscription = this.typeofSubscription[0];
  oneTime = this.typeofSubscription[0];
  orderData:any=[]; 
  imgUrl:any;
  constructor(private router: Router,private panelService:PanelService,private userService:UserService) {
    console.log(this.Subscription)  
    console.log(this.oneTime)  
    this.imgUrl = URLS.productImgUrl
  }

  ngOnInit() {
    let data="on-going"
    if(this.userService.userIsAuthenticated()){

      this.panelService.getOrders(this.category,data).subscribe((res)=>{
        console.log(res['ongoing'])
        this.orderData = res['ongoing'];
        console.log(this.orderData)
      })
    }
  }


  gotoLogin(){
    this.router.navigateByUrl('login')
  }

  checkLogin(){
    if(this.userService.userIsAuthenticated()){
      return true
    }else{
      return false
    }
  }
  openOrderDets(id){
    let data = {
      id:id
    }
    this.router.navigate(['order-dets'],{queryParams:data})
  }

  gotoSubscriptionDetails(id){
    let data ={
      id:id
    }
    this.router.navigate(['subscriptiondets'],{queryParams:data})
  }


  segmentChanged(category){
    if(category==='subscription'){
      let data="on-going"
      this.panelService.getOrders(category,data).subscribe((res)=>{
        console.log(res)
        this.orderData = res['ongoing'];
        console.log(this.orderData)
      })
      console.log(category)
    }else{
      let data="upcomming"
      this.panelService.getOrders(category,data).subscribe((res)=>{
        console.log(res)
        this.orderData = res['upcomming'];
        console.log(this.orderData)
      })
      console.log(category)
    }
    
  }


  selectOptions(category,value){
    console.log(category)
    this.Subscription === value
    // let selectedvalue ="on-going"  
    console.log(this.Subscription)
    if(value=="OnGoing"){
      if(category ==='subscription'){
        let data = 'on-going'
        this.panelService.getOrders(category,data).subscribe((res)=>{
          console.log(res)
          this.orderData = res['ongoing'];
          console.log(this.orderData)
        })
      }else{
        let data = 'upcomming'
        this.panelService.getOrders(category,data).subscribe((res)=>{
          console.log(res)
          this.orderData = res['upcomming'];
          console.log(this.orderData)
        })
      }
      
    }
    else if(value=="Previous"){
      let data = 'previously'
      this.panelService.getOrders(category,data).subscribe((res)=>{
        console.log(res)
        this.orderData = res['privious'];
        console.log(this.orderData)
      }) 
    }
    else if(value=="Cancelled"){
      let data = 'cancelled'
      this.panelService.getOrders(category,data).subscribe((res)=>{
        console.log(res)
        this.orderData = res['cancelled'];
        console.log(this.orderData)
      })
    }

    
  }

  
}
