import { URLS } from './../../services/urls.service';
import { PanelService } from 'src/app/services/panel.service';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ActionSheetController } from '@ionic/angular';
import { Camera } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.page.html',
  styleUrls: ['./myaccount.page.scss'],
})
export class MyaccountPage implements OnInit {
  userDets: any;
  obj:any = {};
  profileImage: any;
  imageUrl = URLS.imageUrl;

  private tutorialHidden: boolean = true;
  constructor(
    private router: Router,
    private activatedRoute:ActivatedRoute,
    private userService:UserService,
    private authService:AuthService,
    private panelService: PanelService,
    private actionSheetController: ActionSheetController,
    private camera: Camera)
  { 
    this.activatedRoute.queryParams.subscribe((res: any) => {
      console.log(res);
      if (res.reload === 'true') {
        this.getUserData();
      }
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    document.getElementById("tutorial").hidden = true;
    console.log("ion view will enter")
    this.getUserData();
  }

  gotoNotification() {
    this.router.navigateByUrl('notifications');
  }

  logout() {
    this.router.navigateByUrl('login');
    this.authService.userLogout();
  }

  getUserData() {
    // this.userDets = this.userService.getUsersData();
    //   console.log(this.userDets);
    //   this.obj = this.userDets;
    //   // console.log(this.obj);
    this.panelService.getUserData().subscribe((res: any) => {
      console.log(res);
      this.obj = res.profile[0];
    }, (err: any) => {
      console.log(err);
    })
  }

  abrirTutorial(){

    if(this.tutorialHidden === true){

      this.tutorialHidden = false;
      document.getElementById("tutorial").hidden = false;

    }else if(this.tutorialHidden === false){

      this.tutorialHidden = true;
      document.getElementById("tutorial").hidden = true;
    }

  }
  gotoEditPage(){
    this.router.navigateByUrl('/edit-page');
  }

  gotoWalletPage(){
    this.router.navigateByUrl('main-page/home/wallet')
  }

  gotoBankTransfer() {
    this.router.navigateByUrl('amount-to-bank')
  }

  gotoVacationPage(){
    this.router.navigateByUrl('myvacation')
  }

  gotoLogin(){
    this.router.navigateByUrl('login')
  }

  userisAuthenticated(){
    return this.userService.userIsAuthenticated()
  }

  async changeProfile() {
    const actionSheet = await this.actionSheetController.create({
      mode: 'md',
      cssClass: 'my-custom-class',
      buttons: [
        {
          text: 'Take Photo',
          icon: 'camera',
          handler: () => {
            this.getFromCamera();
          }
        },
        {
          text: 'Gallery',
          icon: 'image',
          handler: () => {
            this.getFromGallery();
          }
        }
      ]
    });
    await actionSheet.present();
  }
  
  getFromCamera() {
    this.camera.getPicture({
      quality: 50,
      targetWidth: 900,
      targetHeight: 900,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
    }).then((res: any) => {
      console.log(res);
      let base64res = `data:image/png;base64,${res}`;
      this.profileImage = base64res;
      var blob = this.panelService.dataURItoBlob(base64res);
      this.profileImage = { 'base': base64res, 'file': blob };
      console.log(this.profileImage);
    }).catch((err: any) => {
      console.log(err);
    });
  }

  getFromGallery() {
    this.camera.getPicture({
      quality: 50,
      targetWidth: 900,
      targetHeight: 900,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
    }).then((res: any) => {
      console.log(res);
      let base64res = `data:image/png;base64,${res}`;
      this.profileImage = base64res;
      var blob = this.panelService.dataURItoBlob(base64res);
      this.profileImage = { 'base': base64res, 'file': blob };
      console.log(this.profileImage);
    }).catch((err: any) => {
      console.log(err);
    }); 
  }

  uploadProfilePicture() {
    this.panelService.presentLoading();
    this.panelService.uploadProfilePicture(this.profileImage.file).subscribe((res: any) => {
      console.log(res);
      this.panelService.dismissLoading();
    }, (err: any) => {
      console.log(err);
      this.panelService.dismissLoading();
    });
  }

  
checkLogin(){
  if(this.userService.userIsAuthenticated()){
    return true
  }else{
    return false
  }
}
}
