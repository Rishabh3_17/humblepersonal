import { UserService } from './../services/user.service';
import { CartService } from './../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.page.html',
  styleUrls: ['./main-page.page.scss'],
})
export class MainPagePage implements OnInit {
  cartdata:any;
  constructor(private router:Router,private cartService:CartService,private userService:UserService) {
    if(this.userService.userIsAuthenticated()){
      this.getCart()
    }

   }

  ngOnInit() {
  }


  getCart(){
    this.cartService.getCartFromApi().subscribe((res:any)=>{
      console.log(res);
      this.cartdata = res;
      console.log(this.cartdata)
      this.cartService.getCartData = res;
    },(err:any)=>{
      console.log(err)
    })
  }

  


  
  

  cartdatacheck(){
    let response = false;
    // console.log(this.cartService.getCartData)
    // console.log(this.cartService.cartcheckdata())  
    this.cartdata = this.cartService.getCartData
    this.cartService.cartcheckdata().subscribe((res:any)=>{
      // console.log(res)
      response = res
    },(err:any)=>{
      // console.log(err)
    })
    return response
  }

  gotoMyCart(){
    this.router.navigateByUrl('my-cart')
  }
}
