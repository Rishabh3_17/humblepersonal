import { FormGroup } from '@angular/forms';
import { UserService } from './../../../services/user.service';
import { PanelService } from 'src/app/services/panel.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


declare var RazorpayCheckout: any;
@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {
  walletCategory='wallet';
  walletBalance:any = 0.00;
  walletTransaction:any;
  totalAmount:any;
  payMethod:any;
  transactionStatus = "s";
  page;
  form:FormGroup;
  constructor(private router : Router,private activatedRoute:ActivatedRoute,private panelService:PanelService,private userService:UserService) {
    if(this.userService.userIsAuthenticated()){
      this.activatedRoute.params.subscribe(walletBalance=>{
        this.walletBalance = Object.assign(walletBalance) 
        console.log(this.walletBalance)
      })
    }
   }



  ngOnInit() {
    if(this.userService.userIsAuthenticated()){
      this.getTransaction()
    }
    this.form = new FormGroup({
      amount:this.panelService.validatorsFunction()
    })
  }


  checkLogin(){
    if(this.userService.userIsAuthenticated()){
      return true
    }else{
      return false
    }
  }
  gotoLogin(){
    this.router.navigateByUrl('login')
  }
  private tutorialHidden: boolean = true;

  getTransaction(status?, ev?, type?) {
    console.log("Status " + status);
    if (type === "infinite") {
      this.page++;
    } else {
      this.page = 1;
    }
    let tranStatus =this.transactionStatus;
    console.log("Status " + tranStatus);
    this.panelService.getTransaction(tranStatus, this.page).subscribe((res: any) => {
      console.log("Transaction Data");
      console.log(res);
      if (ev) {
        ev.target.complete();
      }
      this.walletTransaction = (type === "infinite") ? [...this.walletTransaction, ...res.wallet_transactions.data] : res.wallet_transactions.data;
      console.log(this.walletTransaction);
    }, (err: any) => {
      if (ev) {
        ev.target.complete();
      }
      console.log(err);
    });
  }

  abrirTutorial() {
    if (this.tutorialHidden === true) {
      this.tutorialHidden = false;
      document.getElementById("tutorial").hidden = false;
    } else if(this.tutorialHidden === false) {
      this.tutorialHidden = true;
      document.getElementById("tutorial").hidden = true;
    }
  }

  gotocheckDets(){
    this.router.navigateByUrl('check-dets')
  }
  gotoBankDets(){
    this.router.navigateByUrl('bank-dets')
  }

  razorSuccessPay(paymentId,userdata) {
    console.log("ye kyu nahi chal raha h");
    console.log(101, paymentId);
    let data = {
      transaction_key: `${paymentId}`,
      amount:this.totalAmount,
      transaction_response:userdata
  };
    // this.loderService.present();
    this.panelService.razorPaySuccessPayment(data).subscribe(
      (response) => {
        // this.loderService.dismiss();
        console.log(435345, response);
        this.router.navigate["/main"];
      },
      (error) => {
        // this.loderService.dismiss();
        // this.panelService.openToast(error.error.message);
        console.log(433, error);
        // alert('error :- ' + JSON.stringify(error));
      }
    );
  }

  async payWithRazor() {
    console.log('rishabh ye chal rha')
    console.log(this.totalAmount);

    let userdata = this.userService.getUsersData()
    let name = `${userdata.first_name} ${userdata.last_name}`;
    let email = userdata.email;
    let contact = userdata.mobile;
    var options = {
      description: `Order id`,
      // image: "https://www.induskargha.com/site/views/images/lyngumlogo1.png",
      // currency: this.currencyCode, // your 3 letter currency code
      key: "rzp_test_oDw757e5auoleh", // your Key Id from Razorpay dashboard
      amount: this.totalAmount * 100, // Payment amount in smallest denomiation e.g. cents for USD
      name: "Humble Milk",
      prefill: {
        email: email,
        contact: contact,
        name: name,
      },
      theme: {
        color: "#F37254",
      },
      modal: {
        ondismiss: function () {
          alert("dismissed");
        },
      },
    };
    console.log(options);
    const razorPayemtId = (paymentId) => this.razorSuccessPay(paymentId,userdata);
    var successCallback = function (payment_id) {
      console.log('ye chal rha h ya nhi betichodAAAA')
      alert("payment_id: " + payment_id);
      console.log(111221122, payment_id);
      razorPayemtId(payment_id);
      return payment_id;
    };
    let routerCopy = this.router;
    var cancelCallback = function (error) {
      console.log(error);
      routerCopy.navigate(["failed-payment"]);
    };
    console.log(RazorpayCheckout)
    console.log('rajorpay bhi chala');
    RazorpayCheckout.open(options, successCallback, cancelCallback);
    console.log(RazorpayCheckout);
  }

  // async payment(payment_mode_id) {
  //   let data = {
  //     payment_mode_id: payment_mode_id,
  //     // currency_code: this.currencyCode,
  //   };
  //   // this.loderService.present();
  //   this.panelService.payment(data).subscribe(
  //     (response) => {
  //       // this.loderService.dismiss();
  //       // this.toastService.openToast(response.message);
  //       console.log(response);
  //       if (payment_mode_id == 2) {
  //         console.log("if block aa gaya");
  //         this.payWithRazor();
  //       } else {
  //         // this.payWithPaypal(); 
  //       }
  //     },
  //     (error) => {
  //       // this.loderService.dismiss();
  //       console.log(error);
  //       // this.toastService.openToast(error.error.message);
  //     }
  //   );
  // }

  // async payNow() {
  //   if (!this.payMethod) {
  //     // this.panelService.showToast("Please select payment method");
  //     return;
  //   }
  //   if (this.payMethod === "RazorPay") {
  //     // this.closeModal();
  //     // await this.payment(2);
  //     await this.payWithRazor()
  //   } 
  // }


  logform(){
    if(this.form.valid){
      this.payWithRazor()
    }else{
      let err = 'Please Enter The Amount'
      this.panelService.errorToast(err)
    }

  }

  doInfinite(ev) {
    this.getTransaction(ev, "infinite");
  }
}
