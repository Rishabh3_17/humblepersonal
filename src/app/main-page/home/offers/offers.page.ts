import { CoupensComponent } from './../../../components/coupens/coupens.component';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OffersComponent } from 'src/app/components/offers/offers.component';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit {
  category ="friends";
  constructor(private modalCtrl:ModalController) { }

  ngOnInit() {
  }

  openoffersModal(){
    this.modalCtrl.create({
      cssClass:'offersModal',
      component:OffersComponent,
      componentProps:{
      }
    }).then((modelEl)=>{
      modelEl.present();
    })
  }


  openCoupensModal(){
    this.modalCtrl.create({
      cssClass:'CoupensModal',
      component:CoupensComponent,
      componentProps:{
      }
    }).then((modelEl)=>{
      modelEl.present();
    })
  }

}
