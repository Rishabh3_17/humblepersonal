import { PanelService } from 'src/app/services/panel.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {
  collection:any;
  page:any;
  constructor(private router : Router,private panelService:PanelService) {
    // this.panelService.presentLoading()
    this.panelService.categoriesApi().subscribe((res)=>{
      this.collection =res['data'].data
      // this.panelService.dismissLoading()
      console.log(this.collection)
    },(err)=>{
      // this.panelService.dismissLoading()
      console.log(err)
    })
   }

  ngOnInit() {
  }

  viewSubCategories(item){
    this.router.navigate(['main-page/home/categories/subcategories',item])
  }

  // Get Categories 
  getCategories(ev?, type?) {
    if(type === "infinite") {
      this.page++;
    } else {
      this.page = 1;
    }
    console.log("page " + this.page);
    this.panelService.categoriesApi(this.page).subscribe((res: any) => {
      if(ev) ev.target.complete();
      console.log("categories");
      console.log(res);
      this.collection = (type === 'infinite') ? [...this.collection, ...res.data.data] : res.data.data;
      // this.collection = res['data'];
      console.log(this.collection);
    }, (err: any) => {
      if(ev) ev.target.complete();
      console.log(err);
    });
  }

  async doInfinite(ev) {
    // console.log(ev);
    this.getCategories(ev, "infinite");
  }
}
