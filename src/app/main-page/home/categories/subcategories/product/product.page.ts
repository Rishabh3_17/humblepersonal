import { PanelService } from 'src/app/services/panel.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
  productData:any = []
  products :any;
  productId:any;
  productType:any;
  page;
  constructor(private router: Router,private activatedRoute:ActivatedRoute,private panelService:PanelService) { }

  ngOnInit() {
    // this.panelService.presentLoading()
    this.activatedRoute.params.subscribe(obj=>{
      let item = Object.assign(obj)
      this.products = item.name;
      this.productId = item.id;
      console.log(item);
      this.getProduct();
    })
  }

  getProduct(ev?, type?) {
    if (type === "infinite") {
      this.page++;
    } else {
      this.page = 1;
    }
    console.log("page " +  this.page);
    this.panelService.getProduct(this.productId, this.page).subscribe((res:any) => {
      if(ev) {
        ev.target.complete();
      }
      console.log(res);
      this.productData = (type === "infinite") ? [...this.productData, ...res.data.data] : res.data.data;
      console.log(this.productData);
    }, (err:any) => {
      if (ev) {
        ev.target.complete();
      }
      console.log(err);
    });
  }

  openSpecificProduct(item){
    console.log(item)
    console.log(this.productType);
    if(this.productType === undefined){
      let message = "Please Select Type"
      this.panelService.errorToast(message) 
    }
    else{    
      let data = {
        productId:item.id,
        productTypeId:this.productType.id,
        productQuantity:this.productType.productQuantity,
        productPrice:this.productType.productprice,
        name:item.name
      }
    console.log(data)
    this.router.navigate(['main-page/home/categories/subcategories/product/specificproduct'],{queryParams:data})
  }
}


optionchange(event){
  let stringsofdata = event.detail.value.split(",");
  this.productType ={
    id:stringsofdata[0],
    productQuantity:stringsofdata[1],
    productprice:stringsofdata[2]
  }
  console.log(this.productType)
}


  goToSearchPage(){
    this.router.navigateByUrl('main-page/home/search')
  }

  doInfinite(ev,) {
    this.getProduct(ev, 'infinite')
  }
}
