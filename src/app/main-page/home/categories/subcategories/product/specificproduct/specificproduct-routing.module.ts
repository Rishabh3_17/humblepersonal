import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SpecificproductPage } from './specificproduct.page';

const routes: Routes = [
  {
    path: '',
    component: SpecificproductPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SpecificproductPageRoutingModule {}
