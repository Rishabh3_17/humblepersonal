import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SpecificproductPage } from './specificproduct.page';

describe('SpecificproductPage', () => {
  let component: SpecificproductPage;
  let fixture: ComponentFixture<SpecificproductPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificproductPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SpecificproductPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
