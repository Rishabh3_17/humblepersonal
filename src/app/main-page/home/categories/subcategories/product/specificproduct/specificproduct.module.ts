import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SpecificproductPageRoutingModule } from './specificproduct-routing.module';

import { SpecificproductPage } from './specificproduct.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SpecificproductPageRoutingModule
  ],
  declarations: [SpecificproductPage]
})
export class SpecificproductPageModule {}
