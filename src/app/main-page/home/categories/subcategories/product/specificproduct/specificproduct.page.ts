import { UserService } from "./../../../../../../services/user.service";
import { async } from "@angular/core/testing";
import { CartService } from "./../../../../../../services/cart.service";
import { ActivatedRoute, Router } from "@angular/router";
import { PanelService } from "src/app/services/panel.service";
import { AddtocartComponent } from "./../../../../../../components/addtocart/addtocart.component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { AlertController, ModalController } from "@ionic/angular";
import * as moment from "moment";
import { URLS } from "src/app/services/urls.service";

@Component({
  selector: "app-specificproduct",
  templateUrl: "./specificproduct.page.html",
  styleUrls: ["./specificproduct.page.scss"],
})
export class SpecificproductPage implements OnInit {
  cartLocalStorageName = "carts";
  count: any = 1;
  delivery_date: any;
  start_date: any;
  end_date: any;
  cartData = {
    items: [],
    total_price: 0,
    total_qty: 0,
  };
  delivery_shift: any;
  item: any;
  id: any;
  name: any;
  productTypeId: any;
  myDate: any;
  orderTypearray = ["One Time Purchase", "Subscription"];
  daysCheckBox = [
    {
      slot: "Sun",
      checked: false,
    },
    {
      slot: "Mon",
      checked: false,
    },
    {
      slot: "Tue",
      checked: false,
    },
    {
      slot: "Wed",
      checked: false,
    },
    {
      slot: "Thu",
      checked: false,
    },
    {
      slot: "Fri",
      checked: false,
    },
    {
      slot: "Sat",
      checked: false,
    },
  ];
  orderType = this.orderTypearray[0];
  checked = [];
  paramsData: any;
  imgUrl :any;
  constructor(
    private cartService: CartService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private modalCtrl: ModalController,
    private panelService: PanelService,
    private userService: UserService,
    private alertCtrl: AlertController
  ) {

    this.imgUrl = URLS.productImgUrl
    // const oldCart = this.cartService.generateId();
    this.activatedRoute.queryParams.subscribe((obj) => {
      console.log(obj);
      this.paramsData = obj;
      this.id = obj.productId;
      this.productTypeId = obj.productTypeId;
      this.name = obj.name;
      console.log(this.productTypeId);
    });
    // this.panelService.presentLoading()
  }

  ngOnInit() {
    this.getproductByID();
  }

  ionViewWillEnter() {}

  getproductByID() {
    // this.panelService.presentLoading()
    console.log(this.id);
    let data = {
      id: this.id,
    };
    this.panelService.getProductById(data).subscribe(
      (res: any) => {
        console.log(res);
        // this.panelService.dismissLoading()
        this.item = res["data"][0];
        console.log(this.item.image);
      },
      (err: any) => {
        console.log(err);
        // this.panelService.dismissLoading()
      }
    );
  }

  selectOptions(value) {
    this.checked = [];
    this.orderType = value;
    console.log(this.orderType);
  }
  selectOption() {
    if (this.orderType === "One Time Purchase") {
      return "o";
    } else if (this.orderType === "Subscription") {
      return "s";
    }
  }

  async addToCart() {
    console.log(this.userService.authenticationState);
    if (this.userService.userIsAuthenticated()) {
      let start = moment(this.start_date).format("MM/DD/YYYY");
      let end = moment(this.end_date).format("MM/DD/YYYY");
      let delivery_date = moment(this.delivery_date).format("MM/DD/YYYY");
      this.start_date;
      console.log(this.productTypeId);
      let ordertype = this.selectOption();
      await this.cartService.addProductWhenUserisLogin(
        this.item,
        this.checked,
        start,
        end,
        ordertype,
        delivery_date,
        this.count,
        this.delivery_shift,
        this.productTypeId
      ).subscribe((res:any)=>{
        console.log(res.message)
        this.panelService.successToast(res.message)
        this.modalCtrl.create({
              cssClass: 'addtocardModal',
              component:AddtocartComponent,
              componentProps:{
              }
            }).then((modalEl)=>{
              modalEl.present();
            })
      },(err:any)=>{
        console.log(err)
        let errvariable = err.error.errors_keys[0]
        console.log(errvariable) 
        this.panelService.errorToast(err.error.errors[errvariable])
      })
    } else {
      this.presentAlertConfirm();
    }

    // this.cartService.getApiresponse(null).subscribe((res:any)=>{
    //   console.log(res)
    //   this.modalCtrl.create({
    //     cssClass: 'addtocardModal',
    //     component:AddtocartComponent,
    //     componentProps:{
    //     }
    //   }).then((modalEl)=>{
    //     modalEl.present();
    //   })
    // },(err:any)=>{
    //   console.log(err)
    // })
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      cssClass: "my-custom-class",
      header: "Please Login",
      message: "You Need To <strong>Login</strong> First!",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: (blah) => {
            this.alertCtrl.dismiss();
          },
        },
        {
          text: "Login",
          handler: () => {
            this.gotoLogin();
            this.alertCtrl.dismiss();
            console.log("Confirm Okay");
          },
        },
      ],
    });

    await alert.present();
  }

  gotoLogin() {
    this.router.navigateByUrl("login");
  }

  // getDeliveryShift(){
  //   if(this.delivery_shift === 'morning'){
  //     return 'm'
  //   }else if(this.delivery_shift === 'evening'){
  //     return 'e'
  //   }
  //   else if(this.delivery_shift === 'both'){
  //     return 'b'
  //   }
  // }
  removeone(count) {
    this.count = this.cartService.removeone(count);
  }
  addone(count) {
    this.count = this.cartService.addone(count);
  }

  //Adds the checkedbox to the array and check if you unchecked it
  addCheckBox(index, checkbox) {
    console.log(checkbox);
    if (checkbox.checked) {
      this.checked.push(checkbox.slot);
      console.log(this.checked);
      this.daysCheckBox[index].checked === true;
    } else {
      let indexinChecked = this.removeCheckedFromArray(checkbox);
      this.daysCheckBox[index].checked === false;
      this.checked.splice(indexinChecked, 1);
      console.log(this.checked);
    }
  }

  //Removes checkbox from array when you uncheck it
  removeCheckedFromArray(checkbox) {
    return this.checked.findIndex((category) => {
      return category === checkbox;
    });
  }

  //Empties array with checkedboxes
  emptyCheckedArray() {
    this.checked = [];
  }

  getCheckedBoxes() {
    //Do whatever
    console.log(this.checked);
  }
}
