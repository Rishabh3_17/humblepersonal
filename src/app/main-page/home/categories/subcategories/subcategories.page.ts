import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PanelService } from 'src/app/services/panel.service';

@Component({
  selector: 'app-subcategories',
  templateUrl: './subcategories.page.html',
  styleUrls: ['./subcategories.page.scss'],
})
export class SubcategoriesPage implements OnInit {
  subCategories : any = [];
  subCategory:any;
  categoryId: any;
  page;
  constructor(private router : Router,private activatedRoute:ActivatedRoute,private panelService:PanelService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(obj =>{
      let item = Object.assign(obj)
      console.log(item)
      this.subCategory = item.name;
      this.categoryId = item.id;
    });    
    this.getSubCategories();
  }

  getSubCategories(ev?, type?) {
    if (type === "infinite") {
      this.page++;
    } else {
      this.page = 1;
    }
    console.log("page " + this.page);
    this.panelService.getSubCategories(this.categoryId, this.page).subscribe((res:any) => {
      if (ev) {
        ev.target.complete();
      }
      console.log(res);
      this.subCategories = (type = "infinite") ? [...this.subCategories, ...res.data.data] : res.data.data;
      console.log(this.subCategories);
    }, (err:any) => {
      if (ev) {
        ev.target.complete();
      }
      console.log(err);
    });
  }

  gotoProduct(product){
    this.router.navigate(['main-page/home/categories/subcategories/product',product])
  }
  
  async doInfinite(ev) {
    // console.log(ev);
    this.getSubCategories(ev, "infinite");
  }
}
