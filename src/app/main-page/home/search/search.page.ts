import { PanelService } from 'src/app/services/panel.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  myInput:any ='';
  searchResult:any;
  searchlength:any;
  filters:any;
  productType:any;
  page;
  constructor(private router:Router,private panelService:PanelService,private activatedRoute:ActivatedRoute) { 
    this.activatedRoute.queryParams.subscribe(data=>{
      // let getdata =JSON.parse(Object.assign(data))
      this.filters = data
      console.log(data)
      this.filterProduct()
    })
    // this.onInputSearch()
  }

  ngOnInit() {
  }

  filterProduct(ev?, type?){
    if (type === "infinite") {
      this.page++
    } else {
      this.page = 1;
    }

    this.panelService.applyFilter(this.filters, this.page).subscribe((res:any) => {
      console.log("Filter Data");
      console.log(res);
      if (ev) {
        ev.target.complete();
      }
      this.searchResult = (type === "infinite") ? [...this.searchResult, ...res.data.data] : res.data.data;
      console.log(this.searchResult)
    }, (err: any) => {
      if (ev) {
        ev.target.complete();
      }
      console.log(err);
    });
  }

  openSpecificProduct(item){ 
    let data = {
      productId:item.id,
      productTypeId:this.productType.id,
      productQuantity:this.productType.productQuantity,
      productPrice:this.productType.productprice,
      name:item.name
    }
    this.router.navigate(['main-page/home/categories/subcategories/product/specificproduct'] ,{
      queryParams:data
    })
  }

  gotofilter(){
    this.router.navigateByUrl('filter');
  }

  optionchange(event){
    let stringsofdata = event.detail.value.split(",");
    this.productType ={
      id:stringsofdata[0],
      productQuantity:stringsofdata[1],
      productprice:stringsofdata[2]
    }
    console.log(this.productType)
  }

  onInputSearch(ev?, type?){
    if (type === "infinite") {
      this.page++
    } else {
      this.page = 1;
    }
    this.panelService.searchProducts(this.myInput).subscribe((res: any) => {
      if (ev) {
        ev.target.complete();
      }
      console.log("Search Data");
      console.log(res);
      this.searchResult = (type === "infinite") ? [...this.searchResult, ...res.data.data] : res.data.data;
      console.log(this.searchResult);
      this.searchlength = this.searchResult.length;
    }, (err: any) => {
      if (ev) {
        ev.target.complete();
      }
      console.log(err);
    });
  }

  doInfinite(ev) {
    console.log(ev);
    this.filterProduct(ev, "infinite");
    this.onInputSearch(ev, "infinite");
  }

}
