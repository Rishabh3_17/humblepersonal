import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BestSellersPage } from './best-sellers.page';

const routes: Routes = [
  {
    path: '',
    component: BestSellersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BestSellersPageRoutingModule {}
