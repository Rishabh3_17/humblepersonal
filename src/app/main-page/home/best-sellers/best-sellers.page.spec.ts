import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BestSellersPage } from './best-sellers.page';

describe('BestSellersPage', () => {
  let component: BestSellersPage;
  let fixture: ComponentFixture<BestSellersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestSellersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BestSellersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
