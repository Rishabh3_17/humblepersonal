import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BestSellersPageRoutingModule } from './best-sellers-routing.module';

import { BestSellersPage } from './best-sellers.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BestSellersPageRoutingModule
  ],
  declarations: [BestSellersPage]
})
export class BestSellersPageModule {}
