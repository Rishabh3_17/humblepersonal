import { URLS } from './../../services/urls.service';
import { UserService } from './../../services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, AnimationController, createAnimation, IonSlides, ModalController } from '@ionic/angular';
import { CoupensComponent } from 'src/app/components/coupens/coupens.component';
import { OffersComponent } from 'src/app/components/offers/offers.component';
import { PanelService } from 'src/app/services/panel.service';
import * as moment from 'moment';

 
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild('slides', { read: IonSlides }) slides: IonSlides;
  GlobalEventHandlers: any;
  footerHidden:any;
  userDetail:any;
  walletBalance:any = 0.00;
  collection:any;
  weekDays=[];
  current:any;
  currentDay:any;
  wallet:any={};
  currentData={
  };
  getDataInParticularDate:any;
  length:any;
  essentials:any;
  bestSeller:any;
  productType:any;
  currentdate:any;
  imgUrl:any;
  // page = 1;
  constructor(private userService: UserService, private router: Router,private modalCtrl: ModalController,private animationCtrl: AnimationController,private panelService:PanelService,private alertController:AlertController) {
    this.userDetail = this.userService.getUsersData()
    console.log(this.userDetail);
    this.getCategories();
    this.getCurrentWeek();
    this.imgUrl = URLS.productImgUrl
   }

  ngOnInit() {
  // for(let i=0;i<=7;i++){
  //   this.weekDays.push({
  //     start_date:moment().add(i, 'days').format('MMMM Do YYYY, h:mm:ss a')
  //   })
  //   console.log(this.weekDays[i])
  //   console.log(moment().weekday())
  //   console.log(this.weekDays)
  // }  
  }


  userisAuthenticated(){
    return this.userService.userIsAuthenticated()
  }

  // Get Categories 
  getCategories() {
    this.panelService.categoriesApi().subscribe((res: any) => {      
      console.log("categories");
      console.log(res);
      this.collection = res.data.data;
      // this.collection = res['data'];
      console.log(this.collection)
    }, (err: any) => {
      console.log(err);
    });
  }

  getCurrentWeek() {
    this.current=  moment().format('MM/DD/YYYY')
    this.currentDay = moment().format('dd') 
    var currentDate = moment();
    var weekStart = currentDate.clone().startOf('week');
    var weekEnd = currentDate.clone().endOf('week');
    var days = [];
    for (var i = 0; i <= 6; i++) {
      this.weekDays.push({
            start_date:moment().add(i, 'days').format('MM/DD/YYYY'),
            day:moment().add(i, 'days').format('dd'),
            time:moment().add(i, 'days').format('h:mm:ss')
          });
          // console.log(this.weekDays[0].start_date)
    }
    console.log({
      message: 'week Days',
      data: this.weekDays
    })
    this.currentData = {
      start_date:this.current,
      day:this.currentDay
    }
  }
  
  ionViewDidEnter(){
    if(this.userisAuthenticated()){
      console.log(' ye chal rha h ')
      this.selectData(this.currentData)
      this.getWalletBalance()
    }
    this.getEssentials()
    this.getBestSellers()
  }
  
  next(){
    let outerDiv = document.getElementById("outerDiv");
    outerDiv.scrollLeft += 62;
  }
    

  selectData(item){
    console.log('ye bhi nahi chal rha')
    let day = item.day
    console.log(day)  
    this.currentdate = item.start_date
    let prevDay = this.currentDay
    document.getElementById(prevDay).classList.remove('daycol-active')
    document.getElementById(day).classList.add('daycol-active')
    this.currentDay = day
    this.panelService.getSubscriptionOnParticularDate(item.start_date).subscribe((res:any)=>{
      console.log(res)
      this.getDataInParticularDate = res.subscription
      console.log(this.getDataInParticularDate.length)
      this.length = this.getDataInParticularDate.length
    })
  }
   
  lengthofdata(){
    if(this.length === 0){
      return true
    }
    else{
      return false
    }
  }

  gotoSearch(){
    this.router.navigateByUrl('main-page/home/search')
  }
        
  openWallet(wallet){
    console.log(wallet)
    this.router.navigate(['main-page/home/wallet' , wallet])
  }

  viewAllCategories(){
    this.router.navigateByUrl('main-page/home/categories')
  }

  viewOffers(){
    this.router.navigateByUrl('main-page/home/offers')
  }
  // openoffersModal(){
  //   this.modalCtrl.create({
  //     cssClass:'offersModal',
  //     component:OffersComponent,
  //     componentProps:{
  //     }
  //   }).then((modelEl)=>{
  //     modelEl.present();
  //   })
  // }

  opensubCategories(item){
    this.router.navigate(['main-page/home/categories/subcategories' , item])
  }

  openCoupensModal(){
    this.modalCtrl.create({
      cssClass:'CoupensModal',
      component:CoupensComponent,
      componentProps:{
      }
    }).then((modelEl)=>{
      modelEl.present();
    })
  }

  
  gotoProduct(){
      this.router.navigateByUrl('main-page/home/categories/subcategories/product')
  }

  gotoEssentials(){
    this.router.navigateByUrl('main-page/home/essentials')
  }
  gotoBestSellers(){
    this.router.navigateByUrl('main-page/home/best-sellers')
  }
  onScroll(event) {
    
    // used a couple of "guards" to prevent unnecessary assignments if scrolling in a direction and the var is set already:
    if (event.detail.deltaY > 0 ) {
      document.getElementById('hideText').classList.add('walletScroll');
    }
    if (event.detail.deltaY < 0) {
      document.getElementById('hideText').classList.remove('walletScroll');
    };
    // if (event.detail.deltaY > 0) {
    //   console.log("scrolling down, hiding footer...");
    //   // this.footerHidden = true;
    //   // var input = document.getElementById('hideText').setAttribute("class", "ff-E-L fs-21 mr-0 hidediv")
    //   document.getElementById('hideText').style.display="none";
    //   createAnimation()
    //   .afterAddClass('d-none')
    //   .duration(1000)
    //   .addAnimation
    // }
    //  else {
    //   console.log("scrolling up, revealing footer...");
    //   // this.footerHidden = false;
    //   document.getElementById('hideText').style.display="block";
    // };
  };


  optionchange(event){
    let stringsofdata = event.detail.value.split(",");
    this.productType ={
      id:stringsofdata[0],
      productQuantity:stringsofdata[1],
      productprice:stringsofdata[2]
    }
    console.log(this.productType)
  }

  getWalletBalance(){
    this.panelService.getWalletBalance().subscribe((res:any)=>{
      console.log(res)
      this.walletBalance = res.balance 
      this.wallet = res;
      // if(this.walletBalance <=0){
      //   // this.presentAlertConfirm()
      // }
    },(err:any)=>{

      console.log(err)
    })
  }


  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'txt-center',
      message: 'Your Account Balance is low </br> <strong class="txt-linkcolor">Please Add Money</strong>!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.alertController.dismiss()
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Add Money',
          cssClass:'primary',
          handler: () => {
            // this.gotoMyWallet()
            this.alertController.dismiss()
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }


  gotoMyWallet(){
    this.router.navigateByUrl('main-page/home/wallet')
  }

  getEssentials() {
    this.panelService.getEssentials().subscribe((res:any)=>{
      this.essentials = res.essential.data 
      console.log(this.essentials)
    },(err:any)=>{
      console.log(err)
    })
  }

  getBestSellers() {
    this.panelService.getBestSellers().subscribe((res:any)=>{
      this.bestSeller = res.bestseller.data;
      console.log(this.bestSeller);
    }, (err) => {
      console.log(err);
    });
  }

  openSpecificProduct(item){
    console.log(item)
    console.log(this.productType);
    if(this.productType === undefined){
      let message = "Please Select Type"
      this.panelService.errorToast(message) 
    }
    else{    
      let data = {
        productId:item.id,
        productTypeId:this.productType.id,
        productQuantity:this.productType.productQuantity,
        productPrice:this.productType.productprice,
        name:item.name
      }
    console.log(data)
    this.router.navigate(['main-page/home/categories/subcategories/product/specificproduct'],{queryParams:data})
  }
}
}
