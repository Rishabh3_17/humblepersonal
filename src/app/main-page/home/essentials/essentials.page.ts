import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PanelService } from 'src/app/services/panel.service';

@Component({
  selector: 'app-essentials',
  templateUrl: './essentials.page.html',
  styleUrls: ['./essentials.page.scss'],
})
export class EssentialsPage implements OnInit {
  productData:any = []
  products :any;
  productType:any;
  constructor(private router: Router,private activatedRoute:ActivatedRoute,private panelService:PanelService) { }

  ngOnInit() {
      this.panelService.getEssentials().subscribe((res:any)=>{
        console.log(res)
        // this.panelService.dismissLoading()
        this.productData = res['essential']['data']
        console.log(this.productData)
      },(err:any)=>{
        this.panelService.errorToast(err.error.message)
        // this.panelService.dismissLoading()
        console.log(err)
      })
  }
  
  openSpecificProduct(item){
    console.log(item)
    console.log(this.productType);
    if(this.productType === undefined){
      let message = "Please Select Type"
      this.panelService.errorToast(message) 
    }
    else{    
      let data = {
        productId:item.id,
        productTypeId:this.productType.id,
        productQuantity:this.productType.productQuantity,
        productPrice:this.productType.productprice,
        name:item.name
      }
    console.log(data)
    this.router.navigate(['main-page/home/categories/subcategories/product/specificproduct'],{queryParams:data})
    this.productType === undefined
  }
}


optionchange(event){
  let stringsofdata = event.detail.value.split(",");
  this.productType ={
    id:stringsofdata[0],
    productQuantity:stringsofdata[1],
    productprice:stringsofdata[2]
  }
  console.log(this.productType)
}



  goToSearchPage(){
    this.router.navigateByUrl('main-page/home/search')
  }

}
