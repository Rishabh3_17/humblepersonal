import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BankDetsPageRoutingModule } from './bank-dets-routing.module';

import { BankDetsPage } from './bank-dets.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BankDetsPageRoutingModule
  ],
  declarations: [BankDetsPage]
})
export class BankDetsPageModule {}
