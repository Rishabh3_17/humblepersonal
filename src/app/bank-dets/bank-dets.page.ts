import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bank-dets',
  templateUrl: './bank-dets.page.html',
  styleUrls: ['./bank-dets.page.scss'],
})
export class BankDetsPage implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  cancelPayment(){
    this.router.navigateByUrl('main-page/home/wallet')
  }

  submitPayment(){
    this.router.navigateByUrl('main-page/home')
  }
}
