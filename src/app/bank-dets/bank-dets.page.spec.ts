import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BankDetsPage } from './bank-dets.page';

describe('BankDetsPage', () => {
  let component: BankDetsPage;
  let fixture: ComponentFixture<BankDetsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankDetsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BankDetsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
