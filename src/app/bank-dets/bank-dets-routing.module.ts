import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BankDetsPage } from './bank-dets.page';

const routes: Routes = [
  {
    path: '',
    component: BankDetsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BankDetsPageRoutingModule {}
