import { UserService } from 'src/app/services/user.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor( private userService:UserService,private router: Router){}

  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
  if(this.userService.userIsAuthenticated()){
    console.log('ye chal rha h')
    this.router.navigateByUrl('main-page/home')
  }
  else{
    console.log('ye wala chal rha h')
    return  !this.userService.userIsAuthenticated()
  }
  
  }  
}
