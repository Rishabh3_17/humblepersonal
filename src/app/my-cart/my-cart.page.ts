import { URLS } from './../services/urls.service';
import { PanelService } from 'src/app/services/panel.service';
import { EditSubscriptionComponent } from './../components/edit-subscription/edit-subscription.component';
import { AlertController, ModalController } from '@ionic/angular';
import { CartService } from './../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-my-cart',
  templateUrl: './my-cart.page.html',
  styleUrls: ['./my-cart.page.scss'],
})

export class MyCartPage implements OnInit {
  form:FormGroup;
  radioValue ="UseCoupon";
  cartData:any;
  ontimeOrders:any=[];
  subscriptionOrders:any=[];
  couponDets:boolean = false;
  totalAmount:any;
  address={
    full_name:'',
    mobile_number:'',
    alternate_mobile_number:'',
    delivery_address:''
  }
  inputTags = [];
  imgUrl:any;
  constructor(private router : Router,private cartService:CartService,private modalCtrl:ModalController,private panelService:PanelService,private alertController:AlertController) {
     this.getCart();
     this.imgUrl = URLS.productImgUrl
   }

  ngOnInit() {
    this.form = new FormGroup({
      full_name:this.panelService.validatorsFunction(),
      mobile_number:this.panelService.validatorsFunction(),
      alternate_mobile_number:this.panelService.validatorsFunction(),
      delivery_address:this.panelService.validatorsFunction()
    }) 
  }

  
  


 async getCart(){
    this.cartService.getCartFromApi().subscribe((res:any)=>{
      console.log(res)
      this.cartService.getCartData = res
      this.totalAmount = res.total_amount;
      console.log(this.totalAmount) 
      this.ontimeOrders = res.one_time_order;
        console.log(this.ontimeOrders);
        this.subscriptionOrders = res.subscription_order;
        console.log(this.subscriptionOrders);
    },(err:any)=>{
      console.log(err)
    })
    // let value = await this.cartService.getcartData
    
  }

  ionViewWillEnter(){
    // this.getCart()
    // await this.getCart()
  }
  
  
  // ionViewDidEnter(){
  //   // this.getcartitems()
   
  // }
  // getcartitems(){
  //   console.log('ye chala')
  //   console.log(this.cartService.getCartData)
  //   this.cartService.observablefunction().subscribe((res:any)=>{
  //     let value = res
  //     console.log(value)
  //     this.totalAmount = value.total_amount;
  //     console.log(value.total_amount) 
  //     this.ontimeOrders = value.items.one_time_orders;
  //       console.log(this.ontimeOrders);
  //       this.subscriptionOrders = value.items.subscription_orders;
  //       console.log(this.subscriptionOrders);
  //   })
  // }


  removeOne(id,index){
    let quantity = parseInt(this.ontimeOrders[index].quantity)
    // this.ontimeOrders[index].quantity = this.cartService.removeone(quantity);
    let data ={
      id:id,
      quantity:quantity-1
    }
    let product_type ='o'
      if(quantity<=1){
        let message = "Cannot Decrease Quantity"
        this.panelService.errorToast(message)
      }else{
        this.cartService.editCartviaapi(data).subscribe((res:any)=>{
           console.log(res)
           this.getCart()
          //  this.getcartitems()
         },(err:any)=>{
           console.log(err)
         })
            
      }
  }
  
  addOne(id,index){
    let quantity = parseInt(this.ontimeOrders[index].quantity)
    // this.ontimeOrders[index].quantity = this.cartService.addone(quantity);
    let data ={
      id:id,
      quantity:quantity+1
    }
    let product_type ='o'
    this.cartService.editCartviaapi(data).subscribe((res:any)=>{
      console.log(res)
      this.getCart()
     //  this.getcartitems()
    },(err:any)=>{
      console.log(err)
    })
    // this.getcartitems()
  }
  
  gotoaddSubscription(){
    this.router.navigateByUrl('addsubscription')
  }

  gotomyLocation(){
    this.router.navigateByUrl('map-location')
  }
  setIcon(){
    console.log(this.radioValue)
  }
  clearCart(){
    this.cartService.clearCart().subscribe((res:any)=>{
      console.log(res)
      this.getCart()
    },(err:any)=>{
      console.log(err)
    })
    
  }
  applyCoupon(){
    if(this.couponDets){
      console.log(this.couponDets)
      this.couponDets = false;
    }else{
      this.couponDets = true;
    }
  }
   removeoneItem(id,type){
    let productType = type
    console.log(id)
    this.cartService.removeItemFromCart(id).subscribe((res:any)=>{
      console.log(res)
      this.getCart()
    },(err:any)=>{
      console.log(err)
    })
  }


  logForm(f){
    if(this.ontimeOrders.length===0 && this.subscriptionOrders.length === 0){
        let message = "No products In Cart"
        this.panelService.errorToast(message)
    }else{
      this.router.navigate(['/checkout'],{
        queryParams:this.address
      })
    }
  }

  openEditSubsModal(id,quantity){
    let quant= parseInt(quantity)
    this.modalCtrl.create({
      cssClass:'editSubscription',
      component:EditSubscriptionComponent,
      componentProps:{
        data:{
          id:id,
          quantity:quant
        }

      }
    }).then((modelEl)=>{
      modelEl.present()
      modelEl.onWillDismiss().then(()=>{
        this.getCart()
      })
    })
  }



  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'txt-center',
      message: 'Are You Sure you want to </br> <strong class="txt-linkcolor">Clear Cart</strong>!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.alertController.dismiss()
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Clear Cart',
          cssClass:'primary',
          handler: () => {
            this.clearCart()
            this.alertController.dismiss()
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}

