import { UserService } from './../services/user.service';
import { AuthService } from './../services/auth.service';
import { SignInWithApple } from "@ionic-native/sign-in-with-apple/ngx";
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { FormGroup } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { Router } from "@angular/router";
import { PanelService } from "../services/panel.service";
import { ForgotPassComponent } from '../components/forgot-pass/forgot-pass.component';

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  formData = {
    email: '',
    password:'',
  };
  userData:any;
  form: FormGroup;
  // forgotData = {
  // }
  constructor(
    private router: Router,
    private modalCtrl: ModalController,
    private panelService: PanelService,
    private googlePlus: GooglePlus,
    private appleSignIn: SignInWithApple,
    private authService:AuthService,
    private userService:UserService
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      email_mobile: this.panelService.validatorsFunction(),
      password: this.panelService.validatorsFunction()
    });
  }

  gotoOtpModal() {}
  
  gotoHome() {
    this.router.navigateByUrl("/main-page/home");
  }

  gotoRegister() {
    this.router.navigateByUrl("register");
  }

  logForm(f) {
    if (this.form.valid) {
      this.authService.userSignin(this.formData).subscribe((res:any)=>{
        console.log(res)
        this.ifUserIsLoginThen(res)
      },(err:any)=>{
        console.log(err)
        this.ifloginGivesError(err)
      })
    } else {
      this.form.markAllAsTouched();
    }
  }

  signinWithGoogle() {
    console.log("ye to chala");
    this.googlePlus
      .login({
        'webClientID':'1082536603378-2m0objimhbbhc8gs7e8vt4425uuvkelr.apps.googleusercontent.com',
        'offline':true
      }).then((response:any)=>{
        let data = {
          social_login_type:'google',
          email:response.email,
          social_login_id:response.userId,
          // first_name:response.givenName,
          // last_name:response.familyName
        }
        console.log(response)
        // this.panelService.presentLoading();
        this.authService.socialLogin(data).subscribe((res:any) => {
          console.log(res);
          // this.panelService.dismissLoading();
          this.ifUserIsLoginThen(res)
        }, (err:any) => {
          console.log(err)
          // this.panelService.dismissLoading();
          this.ifloginGivesError(err)
        })
        console.log('signin ho gya')
      }).catch((err:any)=>{
        console.log('error de diya');

        console.log(err)
      })
  }

  loginWithApple() {
    console.log("ye bhi chala");
    this.appleSignIn
      .signin({})
      .then((res: any) => {
        console.log(res);
      })
      .catch((err: any) => {
        console.log(err);
      });
  }



  ifUserIsLoginThen(res){
    this.userData = res.data;
      this.userService.saveUsersData(this.userData);
      // this.modalCtrl.dismiss();
      this.userService.modifyAuthState();
      let success = "Successfully Login !"
      this.panelService.successToast(success)
      // console.log(this.userService.authenticationState)
      // console.log(this.userService.authenticationState.value);
      this.router.navigate(['/main-page/home'],{
        queryParams:this.userData
      })
      // this.panelService.presentLoading();
  }

  ifloginGivesError(err){
    let error = err.error.errors['error']
    console.log(error)
    this.panelService.errorToast(error)
  }

  gotoForgotPassword(){
    console.log('ye chal rha h')
    this.modalCtrl.create({
      cssClass:'forgotPass',
      component:ForgotPassComponent,
      componentProps:{}
    }).then((modelEL)=>{
      modelEL.present()
    })
  }
}
