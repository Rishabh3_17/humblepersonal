import { Router } from '@angular/router';
import { PanelService } from 'src/app/services/panel.service';
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-filter",
  templateUrl: "./filter.page.html",
  styleUrls: ["./filter.page.scss"],
})
export class FilterPage implements OnInit {
  category = false;
  categories:any=[];
  checked = [];
  range=false;
  subCategory = false;
  selectType = false;
  rangeValues={
    upper:80,
    lower:20
  }
  data = {
    category:[],
    from:'',
    to:''
  }
  daysCheckBox:any =[];
  filterProducts=[];
  page;
  constructor(private panleService:PanelService,private router:Router) {
    // this.panleService.categoriesApi().subscribe((res:any)=>{
    //   this.categories = res['data']
    //   console.log(this.categories)
    // })
    this.getCategories();
    
  }
  ngOnInit() {}
  
  ionViewDidEnter(){
    this.appendData()
  }

  appendData(){
    let lengthofarray = 5;
    var i;
      for(i=0; i<this.categories.length;i++){
        this.daysCheckBox.push({
          category:this.categories[i].name,
          checked:false,
          id:this.categories[i].id
        })
      }
      console.log(this.daysCheckBox)
  }
  
  // Get Categories 
  getCategories(ev?, type?) {
    if(type === "infinite") {
      this.page++;
    } else {
      this.page = 1;
    }
    console.log("page" + this.page);
    this.panleService.categoriesApi(this.page).subscribe((res: any) => {
      if(ev) ev.complete();
      console.log("categories");
      console.log(res);
      this.categories = (type === 'infinite') ? [...this.categories, ...res.data.data] : res.data.data;
      // this.categories = res['data'];
      console.log(this.categories)
    }, (err: any) => {
      console.log(err);
    });
  }
  
  async doInfinite(ev) {
    // console.log(ev);
    this.getCategories(ev, "infinite");
  }

  applyFilter(){
    console.log(this.checked)
   let  data = {
     category:this.checked,
      from:this.rangeValues.lower,
      to:this.rangeValues.upper
    }
    // this.panleService.applyFilter(data).subscribe((res:any)=>{
    //   this.filterProducts = res['data']
    //   console.log(res)
    //   console.log(this.filterProducts)       
    // },(err:any)=>{
    //   console.log(err)
    // })
    this.router.navigate(['main-page/home/search'],{
      queryParams:data
    })
    this.clearFilters()
  }
  
  openDropDown(Category, Categoryid) {
    if (Categoryid === "category") {
      if (Category) {
        this.category = false;
      } else {
        this.category = true;
      }
    } 
    else if(Categoryid === "subCategory"){
      if (Category) {
        this.subCategory = false;
      } else {
        this.subCategory = true;
      }
    } 
    else if(Categoryid === "selectType") {
      if(Category){
        this.selectType = false;
      } else {
        this.selectType = true;
      }
    }
    else{
      if(Category){
        this.range = false;
      } else {
        this.range = true;
      }
    }
  }


  //Adds the checkedbox to the array and check if you unchecked it
addCheckBox(index,checkbox) {
  console.log(checkbox)
    if (checkbox.checked) {
    this.checked.push(checkbox.id);
    console.log(this.checked)
    this.daysCheckBox[index].checked === true;      
  } else {
    let indexinChecked = this.removeCheckedFromArray(checkbox);
    this.daysCheckBox[index].checked === false
    this.checked.splice(indexinChecked,1);
    console.log(this.checked)
  }
}

//Removes checkbox from array when you uncheck it
removeCheckedFromArray(checkbox : String) {
  return this.checked.findIndex((category)=>{
    return category === checkbox;
  })
}

//Empties array with checkedboxes
emptyCheckedArray() {
  this.checked = [];
}

getCheckedBoxes() {
 //Do whatever
 console.log(this.checked);
}

gobacktoSearch(){
  this.router.navigateByUrl('main-page/home/search')
}


clearFilters(){
    console.log(this.daysCheckBox)
    for(let i=0; i<this.categories.length;i++){
      console.log(this.daysCheckBox[i].checked)
      this.daysCheckBox[i].checked = false;        
    }
    this.rangeValues={
      upper:80,
      lower:20
    }
}
}
