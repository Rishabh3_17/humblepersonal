import { URLS } from './../services/urls.service';
import { MapComponent } from './../components/map/map.component';
import { FormGroup } from '@angular/forms';
import { UserService } from './../services/user.service';
import { PanelService } from 'src/app/services/panel.service';
import { CartService } from './../services/cart.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderplacedComponent } from './../components/orderplaced/orderplaced.component';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ShowAddressComponent } from '../components/show-address/show-address.component';



declare var RazorpayCheckout: any;
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {
address:any;
cartData:any;
onetimeOrders:any=[];
subscriptionOrders:any=[];
totalAmount:any;
form:FormGroup; 
data:any;
paymentMethod:any;
imgUrl:any;
  constructor(private modalCtrl: ModalController,private activatedRoute:ActivatedRoute,private cartService:CartService,private router:Router,private panelService:PanelService,private userService:UserService) {
    this.imgUrl = URLS.productImgUrl
    this.getCart();
   }

  ngOnInit() {
  }

  
  gotoorderplaced(){
    if(this.paymentMethod === undefined || this.data === undefined){
      if(this.paymentMethod === undefined){
        let message ="Select Payment Method"
        this.panelService.errorToast(message)
      }
      else if(this.data=== undefined){
        let message ="Select Delivery Address"
        this.panelService.errorToast(message)
      }
    }else{
    console.log(this.paymentMethod)
    let data = {
      payment_type: this.paymentMethod,
      billing_id: this.data.id
    }
    console.log(data)
    // this.panelService.presentLoading()
    this.panelService.checkOut(data).subscribe((res)=>{
      console.log(res)
      this.modalCtrl.create({
        cssClass:'orderPlaced',
        component:OrderplacedComponent,
        componentProps:{
        }
      }).then((modelEl)=>{
        modelEl.present()
      })
      // this.panelService.dismissLoading()
    },(err:any)=>{
      // this.panelService.dismissLoading()
      console.log(err)
    })
  }
}

  getCart(){
    this.cartService.getCartFromApi().subscribe((res:any)=>{
      console.log(res)
      this.totalAmount = res.total_amount;
      console.log(this.totalAmount) 
      this.onetimeOrders = res.one_time_order;
        console.log(this.onetimeOrders);
        this.subscriptionOrders = res.subscription_order;
        console.log(this.subscriptionOrders);
    },(err:any)=>{
      console.log(err)
    })
  }


  removeSelectedAddress(){
    console.log(this.data)
    this.data = undefined;
    console.log(this.data)
  }




  gotoHomepage(){
    this.router.navigateByUrl("main-page/home")
  }



  selectDeliveryAddress(){
    this.modalCtrl.create({
      cssClass:'showAdress',
      component:ShowAddressComponent,
      componentProps:{}
    }).then((modelEl)=>{
      modelEl.present()
      modelEl.onWillDismiss().then((data)=>{
        console.log(data)
        this.data = data.data
        console.log(this.data.name)      
      })        
    })
  }

  


}
